package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AGameWithSticks {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();

        int min = Math.min(n, m);
        out.println(min % 2 == 0 ? "Malvika" : "Akshat");
    }
}
