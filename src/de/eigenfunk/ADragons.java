package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.security.KeyPair;
import java.util.*;

public class ADragons {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int s = in.nextInt();
        int n = in.nextInt();
        Pair[] l = new Pair[n];
        for (int i = 0; i < n; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            l[i] = new Pair(x, y);
        }

        Random rndr = new Random();

        for (int i = 0; i < n; i++) {
            int rnd = rndr.nextInt(n);
            Pair tm = l[i];
            l[i] = l[rnd];
            l[rnd] = tm;
        }

        Arrays.sort(l);

        boolean cand = true;
        for (Pair pair : l) {
            if (pair.x >= s) {
                cand = false;
                break;
            } else {
                s = s + pair.y;
            }
        }

        out.println(!cand ? "NO" : "YES");
    }

    private class Pair implements Comparable<Pair> {
        public int x = 0;

        public int y = 0;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public int compareTo(Pair o) {
            return this.x - o.x;
        }
    }
}
