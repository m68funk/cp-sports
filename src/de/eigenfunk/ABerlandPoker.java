package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class ABerlandPoker {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            int k = in.nextInt();

            int winner = Math.min(m, n / k);

            int rest = m - winner;
            if (rest <= 0) {
                out.println(winner);
            } else {
                int tail = rest / (k - 1);
                if (rest % (k - 1) != 0) {
                    tail++;
                }
                out.println(winner - tail);
            }
        }
    }
}
