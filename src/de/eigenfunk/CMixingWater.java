package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class CMixingWater {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int rr = in.nextInt();
        for (int i = 0; i < rr; i++) {
            long h = in.nextInt();
            long c = in.nextInt();
            long tg = in.nextInt();

            if (tg == h) {
                out.println(1);
            } else if (2 * tg <= c + h) {
                out.println(2);
            } else {
                double m = (h + c) / 2d;
                double tm = tg - m;
                int counter = (int) Math.ceil((h - c) / (2d * tm));
                counter = (counter % 2 != 0) ? counter : counter + 1;
                double toUpper = Math.abs((h - c) / ((double) (2 * (counter - 2))) - tm);
                double toLower = Math.abs((h - c) / ((double) (2 * counter)) - tm);
                if (toUpper <= toLower) {
                    out.println(counter - 2);
                } else {
                    out.println(counter);
                }
            }
        }
    }
}
