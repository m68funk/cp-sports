package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AVasyaAndSocks {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();

        int ct = 0;

        while (n > 0) {
            ct++;
            n--;
            n = ct % m == 0 ? n + 1 : n;
        }
        out.println(ct);
    }
}
