package de.eigenfunk;

import fastIO.FastScanner;
import java.io.PrintWriter;

public class BNewTheatreSquare {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();
            char[][] a = new char[n][m];
            for (int j = 0; j < n; j++) {
                a[j] = in.next().toCharArray();
            }
            y = Math.min(2*x, y);
            int price = 0;
            for (int j = 0; j < n; j++) {
                for (int k = 1; k < m; k++) {
                    if(a[j][k-1] == '.' && a[j][k] == '.' ){
                        a[j][k-1] = 'd';
                        a[j][k] = 'd';
                        price = price + y;
                    }
                }
            }
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    if(a[j][k] == '.'){
                        a[j][k] = 'd';
                        price = price + x;
                    }
                }
            }
            out.println(price);
        }
    }
}
