package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AIceSkating {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int[][] coords = new int[n][2];
        final int x = 0;
        final int y = 1;
        for (int i = 0; i < n; i++) {
            coords[i][x] = in.nextInt();
            coords[i][y] = in.nextInt();
        }
        List[] xs = new List[n];
        List[] ys = new List[n];
        for (int i = 0; i < n; i++) {
            if (xs[coords[i][x]] == null) {
                xs[coords[i][x]] = new LinkedList();
                xs[coords[i][x]].add(i);
            }
            if (ys[coords[i][y]] == null) {
                ys[coords[i][y]] = new LinkedList();
                ys[coords[i][y]].add(i);
            }
        }
        Queue<Integer> queue = new LinkedList<>();
        int[] cluster = new int[n];
        for (int i = 0; i < n; i++) {
            if (cluster[i] == 0) {
                queue.add(i);
                while (!queue.isEmpty()) {
                    
                }
            } else {
                //noop
            }
        }
    }
}
