package de.eigenfunk;

import fastIO.FastScanner;
import java.io.PrintWriter;

public class ANewYearTransportation {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int t = in.nextInt();
        int curr = 1;
        for (int i = 1; i <= n; i++) {
            int ai = in.nextInt();
            if(curr == i){
                curr = i + ai;
            }
            if(curr == t){
                out.println("YES");
                break;
            } else if(curr > t){
                out.println("NO");
                break;
            }
        }
    }
}
