package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BXeniaAndRingroad {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();

        long ct = in.nextInt() - 1;


        for (int i = 1; i < m; i++) {
            int c = in.nextInt();
            long mod = ct % n + 1;
            if (mod > c) {
                ct = ct + n + c - mod;
            } else {
                ct = ct + c - mod;
            }
        }
        out.println(ct);
    }
}
