package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class BPoloThePenguinAndMatrix {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();
        int d = in.nextInt();
        int[] a = new int[n * m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i * m + j] = in.nextInt();
            }
        }
        boolean works = true;
        int mod = a[0] % d;
        for (int i = 1; i < a.length; i++) {
            if (a[i] % d != mod) {
                works = false;
                break;
            }
        }
        if (works) {
            for (int i = 0; i < a.length; i++) {
                a[i] = a[i] / d;
            }
            Random rndr = new Random();
            for (int i = 0; i < a.length; i++) {
                int rnd = rndr.nextInt(n);
                int tm = a[i];
                a[i] = a[rnd];
                a[rnd] = tm;
            }
            Arrays.sort(a);

            int medI = a[(a.length) / 2];

            int res = 0;

            for (int i = 0; i < a.length; i++) {
                res = res + Math.abs(a[i] - medI);
            }

            out.println(res);
        } else {
            out.println("-1");
        }
    }
}
