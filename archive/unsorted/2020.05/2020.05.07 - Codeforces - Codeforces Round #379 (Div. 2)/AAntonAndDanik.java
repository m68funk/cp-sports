package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AAntonAndDanik {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        String games = in.readToken();
        int ct = 0;

        for (int i = 0; i < n; i++) {
            if (games.charAt(i) == 'A') {
                ct++;
            }
        }
        if (n - ct < ct) {
            out.println("Anton");
        } else if (n - ct > ct) {
            out.println("Danik");
        } else {
            out.println("Friendship");
        }
    }
}
