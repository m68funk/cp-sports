package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AEvenOdds {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.readLong();
        long k = in.readLong();
        if (k <= (n / 2 + n % 2)) {
            out.println(k * 2 - 1);
        } else {
            out.println((k - n / 2 - n % 2) * 2);
        }
    }
}
