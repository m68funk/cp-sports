package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.function.IntUnaryOperator;

public class AChewbascaAndNumber {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        char[] x = in.readToken().toCharArray();
        for (int i = 0; i < x.length; i++) {
            int j = Character.getNumericValue(x[i]);
            x[i] = String.valueOf(Math.min(j, 9 - j)).charAt(0);
        }
        if (x[0] == '0') {
            x[0] = '9';
        }
        for (int i = 0; i < x.length; i++) {
            out.print(x[i]);
        }
    }
}
