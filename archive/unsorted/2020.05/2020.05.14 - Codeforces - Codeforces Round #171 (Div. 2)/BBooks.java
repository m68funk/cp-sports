package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BBooks {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int t = in.readInt();
        int[] a = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            a[i] = a[i - 1] + in.readInt();
        }
        int l = 1;
        int r = 1;
        int max = 0;
        while (r <= n) {
            if (a[r] - a[l - 1] <= t) {
                max = Math.max(max, r - l + 1);
                r++;
            } else {
                l++;
            }
        }
        out.println(max);
    }
}
