package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class CBuildingPermutation {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        Random rndr = new Random();

        for (int i = 0; i < n; i++) {
            int rnd = rndr.nextInt(n);
            int tm = a[i];
            a[i] = a[rnd];
            a[rnd] = tm;
        }
        Arrays.sort(a);
        long sum = 0;
        for (int i = 0; i < n; i++) {
            sum = sum + Math.abs(i + 1 - a[i]);
        }
        out.println(sum);
    }
}
