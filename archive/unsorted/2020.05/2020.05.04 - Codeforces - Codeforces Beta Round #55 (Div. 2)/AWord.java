package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AWord {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();
        String sUpper = s.toUpperCase();
        String sLower = s.toLowerCase();
        int sUc = 0;
        int sLc = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == sUpper.charAt(i)) {
                sUc++;
            } else {
                sLc++;
            }
        }
        out.println(sUc > sLc ? sUpper : sLower);
    }
}
