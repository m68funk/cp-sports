package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class CBookReading {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int q = in.nextInt();
        for (int i = 0; i < q; i++) {
            long n = in.nextLong();
            long m = in.nextLong();

            long tail = 0;
            for (int j = 1; j * m <= tail(n, m); j++) {
                tail = tail + (j * m) % 10;
            }
            out.println(base(n, m) + tail);
        }
    }

    private long tail(long n, long m) {
        int sw = (int) (m % 10);
        long res = 0;
        switch (sw) {
            case 1:
            case 7:
            case 3:
            case 9:
                res = (n % (10 * m));
                break;
            case 2:
            case 6:
            case 4:
            case 8:
                res = (n % (5 * m));
                break;
            case 5:
                res = (n % (2 * m));
                break;
            case 0:
                res = 0;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sw);
        }
        return res;
    }

    long base(long n, long m) {
        int sw = (int) (m % 10);
        long res = 0;
        switch (sw) {
            case 1:
            case 3:
            case 7:
            case 9:
                res = (n / (10 * m) * 45);
                break;
            case 2:
            case 4:
            case 6:
            case 8:
                res = (n / (5 * m) * 20);
                break;
            case 5:
                res = (n / (2 * m) * 5);
                break;
            case 0:
                res = 0;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sw);
        }
        return res;
    }
}
