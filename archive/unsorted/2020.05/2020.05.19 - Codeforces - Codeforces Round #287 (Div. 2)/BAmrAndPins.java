package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BAmrAndPins {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        long r = in.nextLong();
        long x1 = in.nextLong();
        long y1 = in.nextLong();
        long x2 = in.nextLong();
        long y2 = in.nextLong();

        long dist2 = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        long ct = 0;
        while (4 * ct * ct * r * r < dist2) {
            ct++;
        }
        out.println(ct);
    }
}
