package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BHanSoloAndLazerGun {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();

        int x0 = in.nextInt();
        int y0 = in.nextInt();

        int[][] xy = new int[n][2];
        for (int i = 0; i < n; i++) {
            xy[i][0] = in.nextInt() - x0;
            xy[i][1] = in.nextInt() - y0;
        }
        int count = 1;
        for (int i = 1; i < n; i++) {
            boolean hit = false;
            for (int j = 0; j < i; j++) {
                if(xy[j][0] * xy[i][1] == xy[i][0] *xy[j][1]){
                    hit = true;
                    break;
                }
            }
            if(!hit){
                count++;
            }
        }
        out.println(count);
    }
}
