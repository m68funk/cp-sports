package de.eigenfunk;

import com.fasterxml.jackson.annotation.JsonBackReference;
import fastIO.FastScanner;

import java.io.PrintWriter;

public class FBinaryStringReconstruction {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n0 = in.nextInt();
            int n1 = in.nextInt();
            int n2 = in.nextInt();

            if (n1 == 0) {
                StringBuilder sb = new StringBuilder();

                if (n0 > 0) {
                    sb.append("0".repeat(n0 + 1));
                }

                if (n2 > 0) {
                    sb.append("1".repeat(n2 + 1));
                }
                out.println(sb.toString());
            } else {
                String res = "";
                for (int j = 0; j <= n1; j++) {
                    res = res + (j % 2 == 0 ? "1" : "0");
                }
                if (n0 > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("0".repeat(n0));
                    res = res.substring(0, 1) + sb.toString() + res.substring(1);
                }
                if (n2 > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("1".repeat(n2));
                    res = sb.toString() + res;
                }
                out.println(res);
            }
        }
    }
}
