package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class ESpecialElements {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int r = 0; r < t; r++) {
            int n = in.nextInt();
            int[] aa = new int[n + 1];
            int max = 0;
            for (int i = 1; i <= n; i++) {
                int myA = in.nextInt();
                max = Math.max(max, myA);
                aa[i] = aa[i - 1] + myA;
            }
            Set<Integer> b = new HashSet();
            for (int i = 1; i < n; i++) {
                for (int j = i + 1; j < n + 1; j++) {
                    int myA = aa[j] - aa[i - 1];
                    if (myA > max) {
                        break;
                    }
                    b.add(myA);
                }
            }
            int counter = 0;
            for (int i = 1; i <= n; i++) {
                if (b.contains(aa[i] - aa[i - 1])) {
                    counter++;
                }
            }
            out.println(counter);
        }
    }
}
