package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class CKThNotDivisibleByN {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int k = in.readInt();

            int itv = k / (n - 1);

            int offset = k - (itv * (n - 1));

            if (offset == 0) {
                out.println(n * itv - 1);
            } else {
                out.println(n * itv + offset);
            }

        }
    }
}
