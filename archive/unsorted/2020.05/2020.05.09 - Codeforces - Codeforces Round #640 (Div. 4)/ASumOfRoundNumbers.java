package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ASumOfRoundNumbers {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int ct = 0;
            String res = "";
            for (int j = 4; j >= 0; j--) {
                int div = (int) Math.pow(10, j);
                int cand = n / div;
                if (cand > 0) {
                    ct++;
                    res = res + cand * div + " ";
                    n = n - cand * div;
                }
            }
            out.println(ct);
            out.println(res);
        }
    }
}
