package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BSameParitySummands {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            long n = in.readLong();
            int k = in.readInt();

            if ((n - (k - 1)) % 2 == 1 && n - (k - 1) > 0) {
                out.println("YES");
                for (int j = 0; j < k - 1; j++) {
                    out.print("1 ");
                }
                out.print(n - (k - 1));
                out.println();
            } else if ((n - 2 * (k - 1)) % 2 == 0 && n - 2 * (k - 1) > 0) {
                out.println("YES");
                for (int j = 0; j < (k - 1) ; j++) {
                    out.print("2 ");
                }
                out.print(n - 2 * (k - 1));
                out.println();
            } else {
                out.println("NO");
            }
        }
    }
}
