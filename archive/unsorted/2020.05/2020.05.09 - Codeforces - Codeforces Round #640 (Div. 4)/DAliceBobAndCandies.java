package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class DAliceBobAndCandies {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int[] c = new int[n];
            for (int j = 0; j < n; j++) {
                c[j] = in.readInt();
            }

            int[] a = new int[n];
            int[] b = new int[n];

            int ap = 0;
            int bp = n - 1;

            a[ap] = c[0];
            ap++;
            int moveCt = 1;

            while (ap <= bp) {
                if (moveCt % 2 == 1) {
                    int target = a[moveCt / 2];
                    int sum = 0;
                    while (sum <= target && ap <= bp) {
                        sum = sum + c[bp];
                        bp--;
                    }
                    b[moveCt / 2] = sum;
                } else {
                    int target = b[moveCt / 2 - 1];
                    int sum = 0;
                    while (sum <= target && ap <= bp) {
                        sum = sum + c[ap];
                        ap++;
                    }
                    a[moveCt / 2] = sum;
                }
                moveCt++;
            }
            int aSum = 0;
            int bSum = 0;
            for (int j = 0; j < n; j++) {
                aSum = aSum + a[j];
                bSum = bSum + b[j];
            }
            out.println(moveCt + " " + aSum + " " + bSum);
        }
    }
}
