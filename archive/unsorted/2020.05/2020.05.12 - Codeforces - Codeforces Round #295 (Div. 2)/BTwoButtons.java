package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BTwoButtons {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int m = in.readInt();

        if (n > m) {
            out.println(n - m);
        } else {
            int counter = 0;
            while (m > n) {
                if (m % 2 == 1) {
                    m = m + 1;
                    counter++;
                }
                m = m / 2;
                counter++;
            }
            counter = counter + n - m;
            out.println(counter);
        }
    }
}
