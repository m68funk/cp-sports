package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class ACAPSLOCK {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();
        if(s.equals(s.toUpperCase())){
            out.println(s.toLowerCase());
        } else if (s.equals(s.substring(0,1).toLowerCase() + s.substring(1).toUpperCase())){
            out.println(s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase());
        } else {
            out.println(s);
        }
    }
}
