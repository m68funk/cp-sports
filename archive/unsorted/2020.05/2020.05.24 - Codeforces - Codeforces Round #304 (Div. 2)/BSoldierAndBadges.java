package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class BSoldierAndBadges {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        Arrays.sort(a);
        int ct = 0;
        int off = a[0];
        for (int i = 0; i < n; i++) {
            if (i + off > a[i]) {
                ct = ct + (i + off - a[i]);
            } else {
                off = a[i] - i;
            }
        }
        out.println(ct);

    }
}
