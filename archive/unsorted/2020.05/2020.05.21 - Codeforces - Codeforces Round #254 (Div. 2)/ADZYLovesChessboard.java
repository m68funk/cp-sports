package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class ADZYLovesChessboard {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();

        String[] f = new String[n];

        for (int i = 0; i < n; i++) {
            f[i] = in.next();
        }

        for (int i = 0; i < n; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < m; j++) {
                char c = f[i].charAt(j);
                if (c == '-') {
                    sb.append(c);
                } else {
                    sb.append((i + j) % 2 == 0 ? 'B' : 'W');
                }
            }
            out.println(sb.toString());
        }
    }
}
