package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BJzzhuAndSequences {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        long x = in.nextLong();
        long y = in.nextLong();

        int n = in.nextInt();

        long mod = 1000000007;

        switch (n % 6) {
            case 0:
                out.println(modul((x - y), mod));
                break;
            case 1:
                out.println(modul(x, mod));
                break;
            case 2:
                out.println(modul(y, mod));
                break;
            case 3:
                out.println(modul((y - x), mod));
                break;
            case 4:
                out.println(modul((-x), mod));
                break;
            case 5:
                out.println(modul((-y), mod));
                break;
        }
    }

    public long modul(long a, long modu) {
        long cand = a % modu;
        return cand < 0 ? cand + modu : cand;
    }
}
