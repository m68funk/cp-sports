package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class CAAndBAndTeamTraining {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();

        int ct = 0;

        while (n > 0 && m > 0) {
            if (n > m) {
                ct++;
                n = n - 2;
                m = m - 1;
            } else if (m > n) {
                ct++;
                n = n - 1;
                m = m - 2;
            } else if (m == n && m > 1) {
                ct++;
                n = n - 1;
                m = m - 2;
            } else {
                n = n - 1;
                m = m - 1;
            }
        }
        out.println(ct);
    }
}
