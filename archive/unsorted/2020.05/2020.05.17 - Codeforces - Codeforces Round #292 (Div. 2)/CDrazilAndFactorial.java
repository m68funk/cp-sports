package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class CDrazilAndFactorial {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        String a = in.readToken();
        int[] d = new int[10];
        for (int i = 0; i < n; i++) {
            switch (a.charAt(i)) {
                case '2':
                    d[2]++;
                    break;
                case '3':
                    d[3]++;
                    break;
                case '5':
                    d[5]++;
                    break;
                case '4':
                    d[2] = d[2] + 2;
                    d[3]++;
                    break;
                case '8':
                    d[2] = d[2] + 3;
                    d[7]++;
                    break;
                case '6':
                    d[5]++;
                    d[3]++;
                    break;
                case '7':
                    d[7]++;
                    break;
                case '9':
                    d[3] = d[3] + 2;
                    d[2] = d[2] + 1;
                    d[7]++;
                    break;
            }
        }
        for (int i = 9; i > 1; i--) {
            for (int j = 0; j < d[i]; j++) {
                out.print(i);
            }
        }
    }
}
