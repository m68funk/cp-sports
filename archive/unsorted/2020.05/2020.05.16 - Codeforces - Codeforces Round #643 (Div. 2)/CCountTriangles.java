package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class CCountTriangles {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int A = in.readInt();
        int B = in.readInt();
        int C = in.readInt();
        int D = in.readInt();
        int ct = 0;
        for (int z = C; z <= D; z++) {
            for(int y = B; y <=C; y++){
                int xt = B - A +1;
                int xMin = z - y +1;
                int xCount = Math.max(0, B - xMin + 1);
                int trueCount = Math.min(xt, xCount);
                ct = ct + trueCount;
            }
        }
        out.println(ct);
    }
}
