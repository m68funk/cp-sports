package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ASequenceWithDigits {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            long a = in.readLong();
            long k = in.readLong();

            long count = 1;
            while (count < k) {
                long add = prod(a);
                if (add > 0) {
                    a = a + add;
                } else {
                    break;
                }
                count++;
            }
            out.println(a);
        }
    }

    public long prod(long l) {
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;

        while (l > 0) {
            long digit = l % 10;

            max = Math.max(max, digit);
            min = Math.min(min, digit);

            l /= 10;
        }

        return min * max;
    }
}
