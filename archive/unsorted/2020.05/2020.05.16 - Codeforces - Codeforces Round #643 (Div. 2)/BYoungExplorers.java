package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BYoungExplorers {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();

        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int[] e = new int[n + 1];
            for (int j = 0; j < n; j++) {
                e[in.readInt()]++;
            }
            int ct = 0;
            int rem = 0;

            for (int j = 1; j <= n; j++) {
                ct = ct + (e[j] + rem) / j;
                rem = (e[j] + rem) % j;
            }
            out.println(ct);
        }
    }
}
