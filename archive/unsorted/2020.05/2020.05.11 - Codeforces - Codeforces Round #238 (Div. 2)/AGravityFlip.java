package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class AGravityFlip {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.readInt();
        }
        Arrays.sort(a);
        for (int i = 0; i < n; i++) {
            out.print(a[i] + " ");
        }

    }
}
