package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BAntonAndCurrencyYouAllKnow {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        char[] r = in.next().toCharArray();
        int l = r.length;
        int[] ri = new int[l];
        for (int i = 0; i < l; i++) {
            ri[i] = Character.getNumericValue(r[i]);
        }
        int index = -1;
        for (int i = 0; i < l; i++) {
            if (ri[i] % 2 == 0 && ri[i] < ri[l - 1]) {
                index = i;
                break;
            } else if (ri[i] % 2 == 0 && ri[i] > ri[l - 1]) {
                index = i;
            }
        }
        if (index == -1 || index == l - 1) {
            out.println("-1");
        } else {
            int tmp = ri[l - 1];
            ri[l - 1] = ri[index];
            ri[index] = tmp;
            for (int i = 0; i < l; i++) {
                out.print(ri[i]);
            }
        }
    }
}
