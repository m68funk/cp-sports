package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ACandies {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int c = in.readInt();
        int f = in.readInt();

        int min = c / f;
        int oneMore = c % f;
        for (int i = 0; i < f; i++) {
            out.print(min + (i < oneMore ? 1 : 0));
            out.print(" ");
        }
    }
}
