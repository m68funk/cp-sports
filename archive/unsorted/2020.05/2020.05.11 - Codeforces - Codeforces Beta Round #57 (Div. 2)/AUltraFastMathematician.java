package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class AUltraFastMathematician {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String fi = in.readToken();
        String si = in.readToken();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < fi.length(); i++) {
            sb.append(fi.charAt(i) == si.charAt(i) ? "0" : "1");
        }
        out.println(sb.toString());
    }
}
