package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AParkLighting {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            if (n % 2 == 0) {
                out.println((n / 2) * m);
            } else if (m % 2 == 0) {
                out.println((m / 2) * n);
            } else {
                out.println((m / 2) * n + n / 2 + 1);
            }
        }
    }
}
