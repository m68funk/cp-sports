package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;

public class BMariaBreaksTheSelfIsolation {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
            }

            Random rndr = new Random();

            for (int k = 0; k < n; k++) {
                int rnd = rndr.nextInt(n);
                int tm = a[k];
                a[k] = a[rnd];
                a[rnd] = tm;
            }

            Arrays.sort(a);

            int res = 1;
            for (int j = n - 1; j >= 0; j--) {
                if (a[j] <= j + 1) {
                    res = j + 2;
                    break;
                }
            }
            out.println(res);
        }
    }
}
