package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.math.BigInteger;

public class CCelexUpdate {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int x1 = in.nextInt();
            int y1 = in.nextInt();
            int x2 = in.nextInt();
            int y2 = in.nextInt();

            int r = x2 - x1;
            int u = y2 - y1;

            if (r == 0 || u == 0) {
                out.println(1);
            } else {
                int res = 0;
                for (int j = (r + u - 1); j >= Math.max(r, u); j--) {
                    res = res + j;
                }
                out.println(res);
            }
        }
    }
}
