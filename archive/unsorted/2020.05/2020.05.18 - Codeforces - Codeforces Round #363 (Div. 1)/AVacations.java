package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AVacations {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int[][] d = new int[n + 1][3];
        for (int i = 1; i <= n; i++) {
            int a = in.nextInt();
            d[i][0] = Math.max(d[i - 1][0], Math.max(d[i - 1][1], d[i - 1][2]));
            switch (a) {
                case 1:
                    d[i][1] = Math.max(d[i - 1][0], d[i - 1][2]) + 1;
                    break;
                case 2:
                    d[i][2] = Math.max(d[i - 1][0], d[i - 1][1]) + 1;
                    break;
                case 3:
                    d[i][2] = Math.max(d[i - 1][0], d[i - 1][1]) + 1;
                    d[i][1] = Math.max(d[i - 1][0], d[i - 1][2]) + 1;
                    break;
            }
        }
        out.println(n - Math.max(d[n][0], Math.max(d[n][1], d[n][2])));
    }
}
