package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class AInsomniaCure {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int k = in.readInt();
        int l = in.readInt();
        int m = in.readInt();
        int n = in.readInt();
        int d = in.readInt();

        int ct = 0;

        for (int i = 1; i <=  d; i++) {
            if(i % k == 0 ||i % l == 0 ||i % m == 0 ||i % n == 0){
                ct++;
            }
        }
        out.println(ct);
    }
}
