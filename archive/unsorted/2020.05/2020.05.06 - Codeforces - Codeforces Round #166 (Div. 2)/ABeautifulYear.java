package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ABeautifulYear {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        for (int i = in.readInt() + 1; i <= 9012; i++) {
            int a = i % 10;
            int b = (i / 10) % 10;
            int c = (i / 100) % 10;
            int d = (i / 1000) % 10;
            if (a != b && a != c && a != d && b != c && b != d && c != d) {
                out.println(i);
                break;
            }
        }
    }
}
