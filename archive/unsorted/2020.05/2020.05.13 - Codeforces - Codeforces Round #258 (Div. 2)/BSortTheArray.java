package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BSortTheArray {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();

        int[] a = new int[n + 2];
        for (int i = 1; i <= n; i++) {
            a[i] = in.readInt();
        }
        a[n + 1] = Integer.MAX_VALUE;

        int l = 1;
        boolean unTouched = true;
        int r = 1;

        boolean p = true;

        for (int i = 1; i < n; i++) {
            if (a[i] > a[i + 1] && unTouched) {
                l = i;
                r = i + 1;
                unTouched = false;
            }
            if (a[i] > a[i + 1]) {
                r = i + 1;
            }
        }

        for (int i = 1; i < l - 1; i++) {
            if (a[i] > a[i + 1]) {
                p = false;
                break;
            }
        }
        for (int i = l; i < r; i++) {
            if (a[i] < a[i + 1]) {
                p = false;
                break;
            }
            if (a[i + 1] < a[l - 1] || a[i] > a[r + 1]) {
                p = false;
                break;
            }
        }
        for (int i = r; i < n; i++) {
            if (a[i] > a[i + 1]) {
                p = false;
                break;
            }
        }


        if (p) {
            out.println("yes");
            out.println(l + " " + r);
        } else {
            out.println("no");
        }
    }
}
