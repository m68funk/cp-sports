package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class CLessOrEqual {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int k = in.nextInt();

        int[] a = new int[n+1];
//        a[n] = Integer.MAX_VALUE;
        a[n] = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        //
        Arrays.sort(a);
        if (k == n) {
            out.println(a[k - 1]);
        } else if (k == 0 && a[0] == 1) {
            out.println("-1");
        } else if (k == 0 && a[0] > 1) {
            out.println("1");
        } else if (k == 0 || a[k - 1] == a[k]) {
            out.println("-1");
        } else if (k > 0) {
            out.println(a[k - 1]);
        }
    }
}
