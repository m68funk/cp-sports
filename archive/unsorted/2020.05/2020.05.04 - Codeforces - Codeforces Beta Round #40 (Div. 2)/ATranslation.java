package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ATranslation {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String t = in.readToken();
        String s = in.readToken();
        boolean trans = true;
        if (t.length() != s.length()) {
            trans = false;
        } else {
            for (int i = 0; i < t.length(); i++) {
                if (t.charAt(i) != s.charAt(s.length() - 1 - i)) {
                    trans = false;
                    break;
                }
            }
        }
        out.println(trans ? "YES" : "NO");
    }
}
