package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ADoubleCola {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.readInt();
        String[] names = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};

        long line = 1;
        long sum_prev = 0;
        long sum = 5;
        while (sum < n) {
            line++;
            sum_prev = sum;
            sum = sum + 5 * (long) Math.pow(2, line - 1);
        }
        int i = (int) ((n - sum_prev - 1) / (long) Math.pow(2, line - 1) % 5);
        out.println(names[i]);
    }
}
