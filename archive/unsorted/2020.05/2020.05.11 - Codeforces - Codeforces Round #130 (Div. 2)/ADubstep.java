package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ADubstep {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String dub = in.readToken();

        out.println(dub.replaceAll("WUB", " ").trim());
    }
}
