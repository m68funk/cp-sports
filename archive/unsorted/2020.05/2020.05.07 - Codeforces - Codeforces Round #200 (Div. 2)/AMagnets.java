package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class AMagnets {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        String m = in.readToken();
        int ct = 1;
        for (int i = 1; i < n; i++) {
            String curr = in.readToken();
            if(m.equals(curr)){
                //nooop
            } else {
                ct++;
                m = curr;
            }
        }
        out.println(ct);
    }
}
