package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.math.BigInteger;

public class BPetrAndACombinationLock {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }

        int limit = 1;
        for (int i = 0; i < n; i++) {
            limit = limit * 2;
        }

        for (int i = 0; i < limit; i++) {
            int curr = 0;
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) != 0) {
                    curr = curr + a[j];
                } else {
                    curr = curr + 360 - a[j];

                }
                curr = curr % 360;
            }
            if(curr == 0){
                out.println("YES");
                return;
            }
        }
        out.println("NO");
    }
}
