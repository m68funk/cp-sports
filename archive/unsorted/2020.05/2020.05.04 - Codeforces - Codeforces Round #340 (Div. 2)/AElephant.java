package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AElephant {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int d = in.readInt();
        out.println(d / 5 + Math.min(1, d % 5));
    }
}
