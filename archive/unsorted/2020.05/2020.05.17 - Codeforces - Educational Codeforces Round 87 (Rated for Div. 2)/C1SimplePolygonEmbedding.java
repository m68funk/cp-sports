package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class C1SimplePolygonEmbedding {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();

            double degr = 90.0d / (double) n;

            double rads = Math.toRadians(degr);
            double c = Math.cos(rads);
            double s = Math.sin(rads);

            double bruch = c / (2 * s);

            out.println(2 * bruch);
        }
    }
}
