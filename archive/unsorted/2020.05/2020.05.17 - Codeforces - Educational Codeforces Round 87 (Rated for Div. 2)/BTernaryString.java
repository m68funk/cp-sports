package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BTernaryString {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            char[] s = in.readToken().toCharArray();
            int[] l1 = new int[s.length];
            int[] l2 = new int[s.length];
            int[] l3 = new int[s.length];

            l1[0] = s[0] == '1' ? 1 : 0;
            l2[0] = s[0] == '2' ? 1 : 0;
            l3[0] = s[0] == '3' ? 1 : 0;

            for (int j = 1; j < s.length; j++) {
                switch (s[j]) {
                    case '1':
                        l1[j] = 1;
                        l2[j] = l2[j - 1] == 0 ? 0 : l2[j - 1] + 1;
                        l3[j] = l3[j - 1] == 0 ? 0 : l3[j - 1] + 1;
                        break;
                    case '2':
                        l1[j] = l1[j - 1] == 0 ? 0 : l1[j - 1] + 1;
                        l2[j] = 1;
                        l3[j] = l3[j - 1] == 0 ? 0 : l3[j - 1] + 1;
                        break;
                    case '3':
                        l1[j] = l1[j - 1] == 0 ? 0 : l1[j - 1] + 1;
                        l2[j] = l2[j - 1] == 0 ? 0 : l2[j - 1] + 1;
                        l3[j] = 1;
                        break;
                }
            }
            if (l1[s.length - 1] == 0 || l2[s.length - 1] == 0 || l3[s.length - 1] == 0) {
                out.println(0);
            } else {
                int min = Integer.MAX_VALUE;
                for (int j = 0; j < s.length; j++) {
                    if (l1[j] > 0 && l2[j] > 0 && l3[j] > 0) {
                        min = Math.min(min, Math.max(Math.max(l1[j], l2[j]), l3[j]));
                    }
                }
                out.println(min);
            }
        }
    }
}
