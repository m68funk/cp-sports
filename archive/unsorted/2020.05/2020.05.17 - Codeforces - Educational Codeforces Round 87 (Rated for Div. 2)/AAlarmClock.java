package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AAlarmClock {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int a = in.readInt();
            int b = in.readInt();
            int c = in.readInt();
            int d = in.readInt();

            long res = 0l;

            if (b >= a) {
                res = b;
            } else {
                res = b;
                if (d >= c) {
                    res = -1;
                } else {
                    long sleep = c - d;
                    long times = (a - b) / sleep;
                    if ((a - b) % sleep > 0) {
                        times++;
                    }

                    res = res + times * c;
                }
            }
            out.println(res);
        }
    }
}
