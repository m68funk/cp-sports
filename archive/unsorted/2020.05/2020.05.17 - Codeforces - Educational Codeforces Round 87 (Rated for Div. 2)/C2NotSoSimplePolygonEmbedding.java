package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class C2NotSoSimplePolygonEmbedding {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();

            double s = Math.sin(Math.PI / (double) (4 * n));

            out.println(1 / (2 * s));
        }
    }
}
