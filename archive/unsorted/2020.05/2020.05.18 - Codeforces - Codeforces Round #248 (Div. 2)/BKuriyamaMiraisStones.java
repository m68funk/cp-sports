package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class BKuriyamaMiraisStones {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        long[] v = new long[n + 1];
        boolean isO = false;
        long[] o = new long[n + 1];
        for (int i = 1; i <= n; i++) {
            v[i] = in.nextInt();
            o[i] = v[i];
        }
        for (int i = 1; i <= n; i++) {
            v[i] = v[i - 1] + v[i];
        }
        int m = in.nextInt();
        for (int i = 0; i < m; i++) {
            int t = in.nextInt();
            int l = in.nextInt();
            int r = in.nextInt();
            if (t == 1) {
                out.println(v[r] - v[l - 1]);
            } else {
                if (!isO) {
                    doO(o);
                    isO = true;
                }
                out.println(o[r] - o[l - 1]);
            }
        }
    }


    private void doO(long[] in) {
        Arrays.sort(in);
        for (int i = 1; i <= in.length - 1; i++) {
            in[i] = in[i - 1] + in[i];
        }
    }
}
