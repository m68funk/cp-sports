package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BSquare {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int a1 = in.readInt();
            int b1 = in.readInt();
            int a2 = in.readInt();
            int b2 = in.readInt();
            if (a1 == a2 && b1 + b2 == a1 ||
                    a1 == b2 && b1 + a2 == a1 ||
                    b1 == a2 && a1 + b2 == b1 ||
                    b1 == b2 && a1 + a2 == b1) {
                out.println("Yes");
            } else {
                out.println("No");
            }
        }
    }
}
