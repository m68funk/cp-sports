package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AABTrialProblem {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            out.println(in.readInt() + in.readInt());
        }
    }
}
