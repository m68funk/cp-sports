package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BCardConstructions {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            long n = in.readLong();

            long count = 0;
            while (n > 1) {
                long sqrt = (long) Math.sqrt(24 * n + 1);
                long h = (sqrt -1) / 6;
                long used = (3 * (h * h) + h) / 2;
                n = n - used;
                count++;
            }
            out.println(count);
        }
    }
}
