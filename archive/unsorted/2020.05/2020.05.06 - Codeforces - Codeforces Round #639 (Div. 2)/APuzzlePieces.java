package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class APuzzlePieces {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();

        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int m = in.readInt();
            if (n == 1 || m == 1 || n == 2 && m == 2) {
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}
