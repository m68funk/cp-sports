package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;

public class CExams {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        AB[] ab = new AB[n];

        for (int i = 0; i < n; i++) {
            int a = in.readInt();
            int b = in.readInt();
            ab[i] = new AB(a, b);
        }
        Arrays.sort(ab);
        int lastTest = 0;
        for (int i = 0; i < n; i++) {
            if (ab[i].b >= lastTest) {
                lastTest = ab[i].b;
            } else {
                lastTest = ab[i].a;
            }
        }
        out.println(lastTest);
    }

    class AB implements Comparable<AB>{
        public int a = 0;
        public int b = 0;

        public AB(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public int compareTo(AB o) {
            if(this.a == o.a) return this.b - o.b;
            return this.a - o.a;
        }
    }
}
