package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BRandomTeams {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.readInt();
        long m = in.readInt();

        long nMax = n - (m - 1);
        long kMax = nMax * (nMax - 1) / 2;

        long nMin = n / m;
        long nRem = n % m;

        long nML = nMin * (nMin - 1) / 2;
        long nMU = nMin * (nMin + 1) / 2;

        long kMin = nRem * nMU + (m - nRem) * nML;

        out.println(kMin + " " + kMax);
    }
}
