package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class DQueue {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] t = new int[n];
        for (int i = 0; i < n; i++) {
            t[i] = in.readInt();
        }
        Arrays.sort(t);
        int ct = 0;
        long sum = 0;
        for (int i = 0; i < n; i++) {
            if(sum <= t[i]){
                ct++;
                sum = sum + t[i];
            }
            if(sum > Integer.MAX_VALUE){
                break;
            }
        }
        out.println(ct);
    }
}
