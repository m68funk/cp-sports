package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CRegistrationSystem {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        Map<String, Integer> m = new HashMap<>();

        for (int i = 0; i < t; i++) {
            String r = in.readToken();
            if(!m.containsKey(r)){
                m.put(r,0);
                out.println("OK");
            } else {
                int ct = m.get(r);
                m.put(r, ++ct);
                out.println(r + ct);
            }
        }
    }
}
