package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BDreamoonAndWiFi {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        char[] s = in.readToken().toCharArray();
        char[] d = in.readToken().toCharArray();
        int target = 0;
        for (int i = 0; i < s.length; i++) {
            switch (s[i]) {
                case '+':
                    target++;
                    break;
                case '-':
                    target--;
                    break;
            }
        }
        double[] first = new double[21];
        first[10] = 1d;
        for (int i = 0; i < d.length; i++) {
            double[] work = new double[21];
            for (int j = 0; j < 21; j++) {
                if (first[j] != 0) {
                    switch (d[i]) {
                        case '+':
                            work[j + 1] = work[j + 1] + first[j];
                            break;
                        case '-':
                            work[j - 1] = work[j - 1] +first[j];
                            break;
                        case '?':
                            work[j + 1] = work[j + 1] +first[j] / 2d;
                            work[j - 1] = work[j - 1] +first[j] / 2d;
                            break;
                    }
                }
            }
            first = work;
        }
        out.println(first[target + 10]);
    }
}
