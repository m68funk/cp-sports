package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class AHulk {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        //"I hate that I love that I hate it"
        String core = "";
        for (int i = 2; i <= n ; i++) {
            if(i % 2 == 0){
                core = core + "that I love ";
            } else {
                core = core + "that I hate ";
            }
        }
        out.println("I hate " + core + "it");
    }
}
