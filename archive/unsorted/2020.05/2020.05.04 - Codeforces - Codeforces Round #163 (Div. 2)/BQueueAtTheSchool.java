package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

public class BQueueAtTheSchool {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int l = in.readInt();
        int t = in.readInt();
        String q = in.readToken();
        char[] prev = q.toCharArray();

        for (int i = 0; i < t; i++) {
            char[] curr = new char[l];
            curr[l - 1] = prev[l - 1];
            for (int j = l - 2; j >= 0; j--) {
                if (prev[j] == 'B' && prev[j + 1] == 'G') {
                    curr[j] = 'G';
                    curr[j + 1] = 'B';
                } else {
                    curr[j] = prev[j];
                }
            }
            prev = curr;
        }
        for (int i = 0; i < l; i++) {
            out.print(prev[i]);
        }
    }
}
