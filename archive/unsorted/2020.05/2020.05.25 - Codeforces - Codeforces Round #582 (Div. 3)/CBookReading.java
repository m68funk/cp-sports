package de.eigenfunk;

import fastIO.FastScanner;
import java.io.PrintWriter;

public class CBookReading {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int q = in.nextInt();
        for (int i = 0; i < q; i++) {
            long n = in.nextLong();
            long m = in.nextLong();

            int sw = (int) (m % 10);
            switch (sw){
                case 1:
                    out.println(n / m * 45);
                    break;
                case 2:
                    out.println(n / m * 20);
                    break;
                case 3:
                    out.println(n / m * 45);
                    break;
                case 4:
                    out.println(n / m * 20);
                    break;
                case 5:
                    out.println(n / m * 5);
                    break;
                case 6:
                    out.println(n / m * 20);
                    break;
                case 7:
                    out.println(n / m * 45);
                    break;
                case 8:
                    out.println(n / m * 20);
                    break;
                case 9:
                    out.println(n / m * 45);
                    break;
            }
        }
    }
}
