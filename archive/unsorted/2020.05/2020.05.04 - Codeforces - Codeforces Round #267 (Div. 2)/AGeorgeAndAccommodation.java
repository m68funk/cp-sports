package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AGeorgeAndAccommodation {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int rooms = in.readInt();
        int count = 0;
        for (int i = 0; i < rooms; i++) {
            if (-in.readInt() + in.readInt() >= 2) {
                count++;
            }
        }
        out.println(count);
    }
}
