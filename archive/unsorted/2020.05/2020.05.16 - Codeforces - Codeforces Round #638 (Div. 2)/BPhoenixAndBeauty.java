package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class BPhoenixAndBeauty {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int k = in.readInt();

            Set<Integer> set = new HashSet();

            for (int j = 0; j < n; j++) {
                set.add(in.readInt());
            }
            if (set.size() > k) {
                out.println("-1");
            } else {
                int[] core = new int[k];
                Arrays.fill(core,1);
                Iterator it = set.iterator();
                int si = set.size();
                while (si-- > 0) {
                    core[si] = (int) it.next();
                }
                Arrays.sort(core);
                int times = Math.min(10000 / k, k * n);
                out.println(times * k);
                for (int j = 0; j < times * k; j++) {
                    out.print(core[j % k] + " ");
                }
                out.println();
            }
        }
    }
}
