package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class BCollectingPackages {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            Pair[] p = new Pair[n];
            for (int j = 0; j < n; j++) {
                p[j] = new Pair(in.nextInt(), in.nextInt());
            }
            Arrays.sort(p);
            StringBuilder res = new StringBuilder();
            int currX = 0;
            int currY = 0;
            boolean d = true;
            for (int j = 0; j < p.length; j++) {
                if (p[j].x < currX) {
                    d = false;
                    break;
                } else {
                    res.append("R".repeat(p[j].x - currX));
                    currX = p[j].x;
                }
                if (p[j].y < currY) {
                    d = false;
                    break;
                } else {
                    res.append("U".repeat(p[j].y - currY));
                    currY = p[j].y;
                }
            }
            if (d) {
                out.println("YES");
                out.println(res.toString());
            } else {
                out.println("NO");
            }
        }
    }

    class Pair implements Comparable<Pair> {
        public final int x;
        public final int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Pair o) {
            if (this.x == o.x) {
                return this.y - o.y;
            } else {
                return this.x - o.x;
            }
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}
