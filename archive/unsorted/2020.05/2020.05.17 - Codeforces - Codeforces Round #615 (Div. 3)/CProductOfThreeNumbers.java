package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class CProductOfThreeNumbers {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        while (t-- > 0) {
            int n = in.readInt();
            StringBuilder ans = new StringBuilder();
            int ct = 0;
            int f = 2;
            while (ct < 3 && f * f <= n) {
                if (n % f == 0) {
                    ct++;
                    n = n / f;
                    ans.append(f);
                    ans.append(" ");
                    if (ct == 2 && n > f) {
                        if (n > f) {
                            ct++;
                            ans.append(n);
                            ans.append(" ");
                        } else {
                            break;
                        }
                    }
                }
                f++;
            }
            if (ct == 3) {
                out.println("YES");
                out.println(ans.toString());
            } else {
                out.println("NO");
            }
        }
    }
}
