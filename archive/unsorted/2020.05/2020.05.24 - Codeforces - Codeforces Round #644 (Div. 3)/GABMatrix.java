package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class GABMatrix {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            int a = in.nextInt();
            int b = in.nextInt();

            if (a * n != b * m) {
                out.println("NO");
            } else {
                out.println("YES");
                for (int j = 0; j < n; j++) {
                    StringBuilder sb = new StringBuilder();
                    for (int k = 0; k < m; k++) {
                        int base = m * m + (k - j * a);
                        sb.append((base % m >= 0 && base % m < a) ? "1" : "0");
                    }
                    out.println(sb.toString());
                }
            }
        }
    }
}
