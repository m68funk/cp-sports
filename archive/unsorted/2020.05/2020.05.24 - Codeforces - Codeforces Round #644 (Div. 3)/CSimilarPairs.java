package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class CSimilarPairs {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] a = new int[n];
            int even = 0;
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
                if (a[j] % 2 == 0) {
                    even++;
                }
            }
            if (even % 2 == 0) {
                out.println("YES");
            } else {
                boolean diffOne = false;
                Arrays.sort(a);
                for (int j = 1; j < n; j++) {
                    if (a[j] - a[j - 1] == 1) {
                        diffOne = true;
                        break;
                    }
                }
                if (diffOne) {
                    out.println("YES");
                } else {
                    out.println("NO");
                }

            }

        }
    }
}
