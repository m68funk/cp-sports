package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class EPolygon {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            String[] m = new String[n];
            for (int j = 0; j < n; j++) {
                m[j] = in.next();
            }
            boolean res = true;
            outer:
            for (int j = 0; j < n - 1; j++) {
                for (int k = 0; k < n - 1; k++) {
                    if (m[j].charAt(k) == '1' && m[j + 1].charAt(k) == '0' && m[j].charAt(k + 1) == '0') {
                        res = false;
                        break outer;
                    }
                }
            }
            out.println(res ? "YES" : "NO");
        }
    }
}
