package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.Arrays;

public class BHonestCoach {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] s = new int[n];
            for (int j = 0; j < n; j++) {
                s[j] = in.nextInt();
            }
            Arrays.sort(s);
            int min = Integer.MAX_VALUE;
            for (int j = 1; j < n; j++) {
                min = Math.min(min, s[j] - s[j - 1]);
            }
            out.println(min);
        }
    }
}
