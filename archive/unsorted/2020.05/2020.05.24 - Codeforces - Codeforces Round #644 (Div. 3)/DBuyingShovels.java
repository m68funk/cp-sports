package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DBuyingShovels {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int k = in.nextInt();

            int sqrt = (int) Math.sqrt(n);
            int res = n;
            for (int j = 1; j <= sqrt && j <= k; j++) {
                if (n % j == 0) {
                    if (n / j <= k) {
                        res = n / j;
                        break;
                    } else {
                        res = j;
                    }
                }
            }
            out.println(n / res);
        }
    }

}
