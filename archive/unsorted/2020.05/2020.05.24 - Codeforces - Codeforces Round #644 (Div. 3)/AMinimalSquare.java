package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AMinimalSquare {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();

            int m = Math.min(a, b);

            int s = Math.max(2 * m, Math.max(a, b));

            out.println(s * s);
        }
    }
}
