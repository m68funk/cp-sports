package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class ACalculatingFunction {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.readLong();
        long res = (n + 1)/2;
        long factor = (n % 2 == 1)? -1: 1;;
        res = res * factor;
        out.println(res);
    }
}
