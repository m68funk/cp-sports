package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class AVanyaAndFence {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int h = in.readInt();
        int ct = 0;
        for (int i = 0; i < n; i++) {
            if(in.readInt() > h){
                ct++;
            }
        }
        out.println(n + ct);
    }
}
