package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AMostUnstableArray {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int m = in.readInt();
            if (n == 1) {
                out.println(0);
            } else {
                long div = m / (n / 2);
                long mod = m % (n / 2);
                long first = div * (n - 1 - (2 * mod));
                long second = (div + 1) * (2 * mod);
                if (n % 2 == 0 && n > 2) {
                    second = second + div;
                }
                out.println(first + second);
            }
        }
    }
}
