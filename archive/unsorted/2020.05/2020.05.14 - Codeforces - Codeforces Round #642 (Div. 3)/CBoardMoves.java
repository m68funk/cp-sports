package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class CBoardMoves {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        long[] ns = new long[5 * 100000];
        for (int i = 0; i < t; i++) {
            int curr = 2;
            ns[2] = 2;
            int n = in.readInt();

            int r = n / 2 + 1;

            for (int j = curr + 1; j <= r; j++) {
                ns[j] = ns[j - 1] + ((long)(j - 1) * 2l * (long)(j - 1));
            }
            out.println(ns[r] * 4l);
        }
    }
}
