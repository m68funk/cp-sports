package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BTwoArraysAndSwaps {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int k = in.readInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.readInt();
            }
            Arrays.sort(a);
            int[] b = new int[n];
            for (int j = 0; j < n; j++) {
                b[j] = in.readInt();
            }
            Arrays.sort(b);
            for (int j = 0; j < k; j++) {
                if (a[j] < b[n - 1 - j]) {
                    a[j] = b[n - 1 - j];
                }
            }


            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum = sum + a[j];
            }
            out.println(sum);
        }
    }
}
