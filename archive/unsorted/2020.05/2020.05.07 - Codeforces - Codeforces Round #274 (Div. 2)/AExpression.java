package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AExpression {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int a = in.readInt();
        int b = in.readInt();
        int c = in.readInt();
        int max = 0;

        max = Math.max(max, a + b + c);
        max = Math.max(max, a * (b + c));
        max = Math.max(max, a * b * c);
        max = Math.max(max, (a + b) * c);

        out.println(max);
    }
}
