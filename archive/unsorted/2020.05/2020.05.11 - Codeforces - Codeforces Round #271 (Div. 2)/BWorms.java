package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BWorms {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int piles = in.readInt();
        int[] a = new int[piles + 1];
        a[0] = 0;
        for (int i = 1; i < piles + 1; i++) {
            a[i] = a[i - 1] + in.readInt();
        }
        int m = in.readInt();
        for (int i = 0; i < m; i++) {
            int q = in.readInt();

            int l = 0;
            int r = piles;
            while (l + 1 < r) {
                int curr = (r + l) / 2;
                if (a[curr] < q) {
                    l = curr;
                } else {
                    r = curr;
                }
            }
            out.println(r);
        }
    }
}
