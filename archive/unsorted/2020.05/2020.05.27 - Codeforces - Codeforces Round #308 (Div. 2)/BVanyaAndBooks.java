package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class BVanyaAndBooks {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        long n = in.nextInt();

        long digits = 1;
        long lower = 0;
        long upper = 9;

        long res = 0;

        while (n >= upper) {
            res = res + (upper - lower) * digits;
            lower = upper;
            upper = upper * 10 +9;
            digits++;
        }

        res = res + (n - lower) * digits;

        out.println(res);
    }
}
