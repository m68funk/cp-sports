package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ANearlyLuckyNumber {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long l = in.readLong();

        int count = 0;
        while (l > 0) {
            if (l % 10 == 4 || l % 10 == 7) {
                count++;
            }
            l = l / 10;
        }
        boolean lc = (count == 4 || count == 7);
        out.println(lc ? "YES" : "NO");
    }
}
