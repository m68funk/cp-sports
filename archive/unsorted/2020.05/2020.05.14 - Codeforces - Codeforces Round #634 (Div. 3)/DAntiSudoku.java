package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class DAntiSudoku {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int r = 0; r < t; r++) {
            String[] s = new String[9];
            for (int i = 0; i < 9; i++) {
                s[i] = in.readToken();
            }
            for (int i = 0; i < 9; i++) {
                int coord = (i % 3) * 3 + (i / 3);
                char ch = s[i].charAt(coord);
                char chN = (char) (ch + 1);
                if (chN == ':') {
                    chN = '1';
                }
                s[i] = s[i].replace(ch, chN);
            }
            for (int i = 0; i < 9; i++) {
                out.println(s[i]);
            }
        }
    }
}
