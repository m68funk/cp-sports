package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class APresents {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] from = new int[n];
        for (int i = 0; i < n; i++) {
            from[in.readInt() - 1] = i + 1;
        }

        for (int i = 0; i < n; i++) {
            out.print(from[i] + " ");
        }
    }
}
