package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AHQ9 {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String what = in.readToken();
        if (what.contains("H") || what.contains("Q") || what.contains("9")) {
            out.println("YES");
        } else {
            out.println("NO");
        }
    }
}
