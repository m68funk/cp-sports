package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ANewYearAndHurry {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int k = in.readInt();
        out.println(Math.min(n, (int) Math.floor(Math.sqrt((2 * (240 - k)) / 5d + 0.5d) - 0.5)));
    }
}
