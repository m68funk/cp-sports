package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;

public class APuzzles {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int m = in.readInt();

        int[] f = new int[m];
        for (int i = 0; i < m; i++) {
            f[i] = in.readInt();
        }
        Arrays.sort(f);

        int diff = Integer.MAX_VALUE;

        for (int i = n - 1; i < m; i++) {
            diff = Math.min(diff, Math.abs(f[i] - f[i - n + 1]));
        }

        out.println(diff);
    }
}
