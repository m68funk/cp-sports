package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class APashmakAndGarden {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int x1 = in.readInt();
        int y1 = in.readInt();
        int x2 = in.readInt();
        int y2 = in.readInt();


        int x3 = 0;
        int y3 = 0;
        int x4 = 0;
        int y4 = 0;

        boolean same = x1 == x2 && y1 == y2;

        boolean noSquare = x1 != x2 && y1 != y2 && Math.abs(x1 - x2) != Math.abs(y1 - y2);

        if (same || noSquare) {
            out.println("-1");
        } else {
            if (x1 == x2) {
                x3 = x1 + y2 - y1;
                y3 = y1;
                x4 = x1 + y2 - y1;
                y4 = y2;
            } else if (y1 == y2) {
                y3 = y1 + x2 - x1;
                x3 = x1;
                y4 = y1 + x2 - x1;
                x4 = x2;
            } else {
                x3 = x1;
                y3 = y2;
                x4 = x2;
                y4 = y1;
            }
            out.println(x3 + " " + y3 + " " + x4 + " " + y4);
        }
    }
}
