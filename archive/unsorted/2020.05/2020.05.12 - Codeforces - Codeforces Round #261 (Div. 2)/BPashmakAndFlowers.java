package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BPashmakAndFlowers {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        long min = Long.MAX_VALUE;
        long minCt = 0;

        long max = Long.MIN_VALUE;
        long maxCt = 0;
        long b;
        for (int i = 0; i < n; i++) {
            b = in.readInt();

            if (b < min) {
                min = b;
                minCt = 1;
            } else if (b == min) {
                minCt++;
            }
            if (b > max) {
                max = b;
                maxCt = 1;
            } else if (b == max) {
                maxCt++;
            }
        }
        if (max == min) {
            out.println(0 + " " + ((minCt - 1) * (maxCt) / 2));
        } else {
            out.println((max - min) + " " + (minCt * maxCt));
        }
    }
}
