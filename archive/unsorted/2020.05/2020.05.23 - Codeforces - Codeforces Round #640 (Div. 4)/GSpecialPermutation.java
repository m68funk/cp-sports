package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class GSpecialPermutation {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            if (n < 4) {
                out.println("-1");
            } else {
                StringBuilder sb = new StringBuilder();
                for (int j = ((n / 2) * 2); j > 5; j = j - 2) {
                    sb.append(j + " ");
                }
                sb.append("3 1 4 2 ");
                for (int j = 5; j <= n; j = j + 2) {
                    sb.append(j + " ");
                }
                out.println(sb.toString());
            }
        }
    }
}
