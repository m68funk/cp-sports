package de.eigenfunk;

import java.util.Arrays;

public class Speed {
    public static void main(String[] args) {
        int n = 20000000;

        for (int i = n; i < n + 10; i++) {

            doIt(i);

        }
    }

    private static void doIt(int n) {
        int[] arr = new int[n];

        System.out.println("init start");
        for (int i = 0; i < n; i++) {
            arr[i] = 1;
//            if (i % 2 == 0) {
//                arr[i] = i;
//            } else {
//                arr[i] = n - i;
//            }
        }
        arr[n -1] = Integer.MAX_VALUE;
//        arr[n -1] = Integer.MIN_VALUE;
//        arr[n -1] = 0;
        System.out.println("init done");

        long start = System.currentTimeMillis();
        Arrays.sort(arr);
        System.out.println(arr.length + ">" + (System.currentTimeMillis() - start));
    }
}
