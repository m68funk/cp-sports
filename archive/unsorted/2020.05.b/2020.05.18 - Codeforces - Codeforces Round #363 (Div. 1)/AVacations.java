package de.eigenfunk;

import fastIO.FastScanner;

import java.io.PrintWriter;

public class AVacations {
    public void solve(int testNumber, FastScanner in, PrintWriter out) {
        int n = in.nextInt();
        boolean[] g = new boolean[n + 1];
        boolean[] c = new boolean[n + 1];

        for (int i = 1; i <= n; i++) {
            switch (in.nextInt()) {
                case 0:
                    break;
                case 1:
                    c[i] = true;
                    break;
                case 2:
                    g[i] = true;
                    break;
                case 3:
                    g[i] = true;
                    c[i] = true;
                    break;
            }
            if (g[i - 1] && g[i] && !c[i - 1] && !c[i]) {
                g[i] = false;
            }
            if (g[i - 1] && g[i] && c[i - 1] && !c[i]) {
                g[i - 1] = false;
            }
            if (g[i - 1] && g[i] && !c[i - 1] && c[i]) {
                g[i] = false;
            }
            if (g[i - 1] && g[i] && c[i - 1] && c[i]) {
                g[i] = false;
                c[i - 1] = false;
            }
            if (!g[i - 1] && !g[i] && c[i - 1] && c[i]) {
                c[i] = false;
            }
            if (g[i - 1] && !g[i] && c[i - 1] && c[i]) {
                c[i - 1] = false;
            }
            if (!g[i - 1] && g[i] && c[i - 1] && c[i]) {
                c[i] = false;
            }
            if (i < n) {
                if (!g[i - 1] && g[i] && !c[i - 1] && c[i]) {
                    if (!g[i + 1] && c[i + 1]) {
                        c[i] = false;
                    }
                    if (g[i + 1] && !c[i + 1]) {
                        g[i] = false;
                    }
                }
            }
        }
        int ct = 0;
        for (int i = 1; i <= n; i++) {
            if (!g[i] && !c[i]) {
                ct++;
            }
        }
        out.println(ct);
    }
}
