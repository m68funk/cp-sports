package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AMaximumIncrease {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int sub = 1;
        int count =1;
        int previous = in.nextInt();
        int current;
        for (int i = 1; i < n; i++) {
            current = in.nextInt();
            if (previous < current){
                count++;
                sub = Math.max(sub, count);
            } else {
                count = 1;
            }
            previous = current;
        }
        out.println(sub);
    }
}
