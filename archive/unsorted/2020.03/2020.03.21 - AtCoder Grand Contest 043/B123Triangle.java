package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class B123Triangle {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        in.nextLine();
        String inp = in.nextLine();
        int[] xs = new int[n];
        for (int i = 0; i < n; i++) {
            xs[i] = inp.charAt(i) - 48;
        }
        for (int i = 2; i <= n; i++) {
            for (int j = 0; j <= n - i; j++) {
                xs[j] = Math.abs(xs[j] - xs[j + 1]);
            }
        }
        out.println(xs[0]);
    }
}
