package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AGradeAllocation {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int s = in.nextInt();
            int max = in.nextInt();
            int sum = 0;
            for (int j = 0; j < s; j++) {
                sum = sum + in.nextInt();
            }
            out.println(Math.min(max, sum));
        }
    }
}
