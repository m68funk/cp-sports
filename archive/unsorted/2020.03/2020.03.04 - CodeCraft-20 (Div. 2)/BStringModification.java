package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BStringModification {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int l = in.nextInt();
            in.nextLine();
            String s = in.nextLine();

            int k = 1;
            String curr = s;
            for (int j = 2; j <= s.length(); j++) {
                String head = s.substring(j -1);

                String tail = s.substring(0, j-1);

                if (head.length() % 2 == 1) {
                    tail = new StringBuilder(tail).reverse().toString();
                }

                String cand = head + tail;


                if (cand.compareTo(curr) < 0) {
                    k = j;
                    curr = cand;
                }
            }

            out.println(curr);
            out.println(k);
        }
    }
}
