package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class CAddingPowers {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int T = in.nextInt();

        for (int i = 0; i < T; i++) {
            int n = in.nextInt();
            int k = in.nextInt();
            long[] as = new long[n];
            for (int j = 0; j < n; j++) {
                as[j] = in.nextLong();
            }

            Arrays.sort(as);
            if (as[as.length - 1] == 0) {
                out.println("YES");
            } else {
                int step = 0;
                while (Math.pow(k, step) < as[as.length - 1]) {
                    step++;
                }



                long[] copy = Arrays.copyOf(as, as.length);
                for (int j = step; j >= 0; j--) {
                    long pow = (long) Math.pow(k, j);


                    for (int l = copy.length - 1; l >= 0; l--) {
                        if(copy[l] - pow >= 0){
                            copy[l] = copy[l] - pow;
                            break;
                        }

                    }


                }




                boolean works =true;
                for (int j = 0; j < copy.length; j++) {
                    if(copy[j] !=0){
                        works = false;
                    }
                }
                if(works){
                    out.println("YES");
                }else {
                    out.println("NO");
                }
            }
        }
    }
}
