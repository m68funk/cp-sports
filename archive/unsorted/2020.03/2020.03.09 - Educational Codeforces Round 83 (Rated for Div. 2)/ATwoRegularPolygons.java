package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ATwoRegularPolygons {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            if(n % m == 0){
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}
