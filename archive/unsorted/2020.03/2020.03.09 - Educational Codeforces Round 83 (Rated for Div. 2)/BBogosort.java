package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BBogosort {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] as = new int[n];
            for (int j = 0; j < n; j++) {
                as[j] = in.nextInt();
            }
            Arrays.sort(as);
            for (int j = as.length - 1; j >= 0; j--) {
                out.write(as[j] + " ");
            }
            out.println();
        }
    }
}
