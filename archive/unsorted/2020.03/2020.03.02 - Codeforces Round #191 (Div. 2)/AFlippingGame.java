package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AFlippingGame {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        int[] sx = new int[t];
        int S = in.nextInt();
        sx[0] = S == 0 ? 1 : -1;
        for (int i = 1; i < t; i++) {
            int ni = in.nextInt();
            int niGain = ni == 0 ? 1 : -1;
            S = S + ni;
            sx[i] = Math.max(sx[i - 1] + niGain, niGain);
        }
        int gain = -1;
        for (int i = 0; i < t; i++) {
            gain = Math.max(gain, sx[i]);
        }
        out.println(S + gain);
    }

}
