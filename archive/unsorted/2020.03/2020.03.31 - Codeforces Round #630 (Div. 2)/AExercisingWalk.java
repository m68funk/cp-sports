package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AExercisingWalk {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            long outCount = 0;
            long a = in.nextLong();
            long b = in.nextLong();
            long c = in.nextLong();
            long d = in.nextLong();

            long x = in.nextLong();
            long y = in.nextLong();

            long x1 = in.nextLong();
            long y1 = in.nextLong();

            long x2 = in.nextLong();
            long y2 = in.nextLong();

            outCount = ((x1 <= x - Math.min(a,1) && x - Math.min(a,1) <= x2) || (x1 <= x + Math.min(b,1) && x + Math.min(b,1) <= x2)) && (x1 <= x - a + b && x - a + b <= x2) ? outCount : outCount + 1;
            outCount = ((y1 <= y - Math.min(c,1) && y - Math.min(c,1) <= y2) || (y1 <= y + Math.min(d,1) && y + Math.min(d,1) <= y2)) && (y1 <= y - c + d && y - c + d <= y2) ? outCount : outCount + 1;


            if (outCount == 0) {
                out.println("Yes");
            } else {
                out.println("No");
            }
        }
    }
}
