package main;

import java.util.*;
import java.io.PrintWriter;

public class BCompositeColoring {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        int[] prims = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] a = new int[n];
            Set<Integer> colors = new HashSet<>();
            for (int j = 0; j < n; j++) {
                int ai = in.nextInt();
                for (int k = 0; k < prims.length; k++) {
                    if (ai % prims[k] == 0) {
                        a[j] = k + 1;
                        colors.add(k);
                        break;
                    }
                }
            }
            out.println(colors.size());
            ArrayList<Integer> colorsList = new ArrayList<>(colors);
            for (int j = 0; j < n; j++) {
                out.print(colorsList.indexOf(a[j] - 1) + 1 + " ");
            }
            out.println();
        }
    }
}
