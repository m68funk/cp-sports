package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BGrowTheTree {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int[] sticks = new int[n];

        for (int i = 0; i < n; i++) {
            sticks[i] = in.nextInt();
        }

        Arrays.sort(sticks);

        int half = n / 2;

        long shrt = 0;

        long lng = 0;

        for (int i = 0; i < half; i++) {
            shrt = shrt + sticks[i];
        }
        for (int i = half; i < n; i++) {
            lng = lng + sticks[i];
        }

        out.println(shrt * shrt + lng * lng);
    }
}
