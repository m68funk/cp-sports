package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AIntegerPoints {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            long n = in.nextInt();
            long oddAs = 0;
            for (int j = 0; j < n; j++) {
                if (in.nextInt() % 2 == 1) oddAs++;
            }

            long m = in.nextInt();
            long oddBs = 0;
            for (int j = 0; j < m; j++) {
                if (in.nextInt() % 2 == 1) oddBs++;
            }

            out.println(n * m - (oddAs * (m - oddBs)) - (oddBs * (n - oddAs)));
        }
    }
}
