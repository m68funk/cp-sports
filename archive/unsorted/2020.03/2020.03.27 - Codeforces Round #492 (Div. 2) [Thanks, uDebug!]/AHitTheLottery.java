package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AHitTheLottery {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int count = 0;
        count = count + n / 100;
        n = n - (n / 100) * 100;
        count = count + n / 20;
        n = n - (n / 20) * 20;
        count = count + n / 10;
        n = n - (n / 10) * 10;
        count = count + n / 5;
        n = n - (n / 5) * 5;
        count = count + n;
        out.println(count);
    }
}
