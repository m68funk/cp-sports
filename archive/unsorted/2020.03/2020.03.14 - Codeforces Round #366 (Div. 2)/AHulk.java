package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AHulk {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        StringBuilder res = new StringBuilder();
        for (int i = n; i >= 1; i) {
            if (i == 1){
                res.append("I hate it");
            } else if( i % 2 ==0){
                res.append("I hate that ");
            } else if(i % 2 == 1){
                res.append("I hate that ");
            }
        }
    }
}
