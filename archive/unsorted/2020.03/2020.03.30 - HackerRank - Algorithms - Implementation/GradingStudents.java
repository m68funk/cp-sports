package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class GradingStudents {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int grade = in.nextInt();
            if (grade % 5 > 2 && grade >= 38) {
                grade = 5 * (grade / 5) + 5;
            }
            out.println(grade);
        }
    }
}
