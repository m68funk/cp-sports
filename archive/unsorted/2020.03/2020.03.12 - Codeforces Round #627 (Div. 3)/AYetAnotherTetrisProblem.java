package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AYetAnotherTetrisProblem {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();


            boolean allOdd = true;
            boolean allEven = true;

            for (int j = 0; j < n; j++) {
                if(in.nextInt() % 2 == 0){
                    allOdd = false;
                } else {
                    allEven= false;
                }
            }
            if (allOdd || allEven) {
                out.println("YES");
            } else {
                out.println("NO");
            }

        }
    }
}
