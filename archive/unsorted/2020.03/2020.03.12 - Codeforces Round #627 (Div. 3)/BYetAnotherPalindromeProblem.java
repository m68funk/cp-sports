package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BYetAnotherPalindromeProblem {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();

            int[] aloc = new int[n+1];
            Arrays.fill(aloc, -1);
            boolean hasPalindrome = false;

            for (int j = 0; j < n; j++) {
                int ai = in.nextInt();
                if(aloc[ai]  == -1){
                    aloc[ai] = j;
                } else if(aloc[ai] + 2 <= j){
                    hasPalindrome = true;
                }
            }
            if(hasPalindrome){
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }
}
