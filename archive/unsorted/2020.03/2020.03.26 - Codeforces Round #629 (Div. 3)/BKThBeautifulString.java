package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BKThBeautifulString {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            long k = in.nextLong();

            long left = (int) Math.ceil(Math.sqrt(2 * k + 0.25) + 0.5);
            long right = k - ((left - 1) * (left - 2)) / 2;

            StringBuilder sb = new StringBuilder(n);
            for (int j = 0; j < n; j++) {
                if (j == n - left || j == n - right) {
                    sb.append("b");
                } else {
                    sb.append("a");
                }
            }
            out.println(sb.toString());
        }
    }
}
