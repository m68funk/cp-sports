package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ADivisibilityProblem {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();

            if (a % b == 0) {
                out.println("0");
            } else {
                out.println((((a / b) + 1) * b) - a);
            }
        }
    }
}
