package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class CTernaryXOR {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int digits = in.nextInt();
            in.nextLine();
            String xString = in.nextLine();
            int[] x = new int[digits];
            for (int j = 0; j < digits; j++) {
                x[j] = xString.charAt(j) - 48;
            }
            int[] a = new int[digits];
            int[] b = new int[digits];

            boolean goMax = false;
            for (int j = 0; j < digits; j++) {
                if (x[j] == 0) {
                    a[j] = 0;
                    b[j] = 0;
                } else if (x[j] == 1) {
                    if (goMax) {
                        a[j] = 0;
                        b[j] = 1;
                    } else {
                        a[j] = 1;
                        b[j] = 0;
                        goMax = true;
                    }

                } else if (x[j] == 2) {
                    if (goMax) {
                        a[j] = 0;
                        b[j] = 2;
                    } else {
                        a[j] = 1;
                        b[j] = 1;
                    }
                }
            }
            for (int j = 0; j < digits; j++) {
                out.print(a[j]);
            }
            out.println();
            for (int j = 0; j < digits; j++) {
                out.print(b[j]);
            }
            out.println();
        }
    }
}
