package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AQAQ {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        char[] str = in.nextLine().toCharArray();
        int q = 0;
        int qa = 0;
        int qaq = 0;
        for (int i = 0; i < str.length; i++) {
            if (str[i] == 'Q') {
                q++;
                qaq = qaq + qa;
            } else if (str[i] == 'A' && q > 0) {
                qa = qa + q;
            }
        }
        out.println(qaq);
    }
}
