package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AFillingShapes {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int res = n % 2 == 1 ? 0 : (int) Math.pow(2, n / 2);
        out.println(res);
    }
}
