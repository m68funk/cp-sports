package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class CFrogJumps {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            String str = in.next();

            int d =1;
            int max = 1;
            for (int j = 0; j < str.length() ; j++) {
                char c = str.charAt(j);
                if(c == 'L'){
                    d++;
                    max = Math.max(max, d);
                } else {
                    d = 1;
                }
            }

            out.println(max);
        }
        out.flush();
    }
}
