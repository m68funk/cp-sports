package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class CAirConditioner {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int q = in.nextInt();

        for (int i = 0; i < q; i++) {
            int n = in.nextInt();
            int initialT = in.nextInt();

            int[] times = new int[n];
            int[] lows = new int[n];
            int[] highs = new int[n];
            for (int j = 0; j < n; j++) {
                times[j] = in.nextInt();
                lows[j] = in.nextInt();
                highs[j] = in.nextInt();
            }
            int currT = 0;
            int currMin = initialT;
            int currMax = initialT;

            boolean possible = true;
            for (int j = 0; j < n; j++) {
                int since = times[j] - currT;
                currMin = currMin - since;
                currMax = currMax + since;
                if(currMax < lows[j] || currMin > highs[j]){
                    possible = false;
                    break;
                }
                currT = times[j];
                currMin = Math.max(currMin, lows[j]);
                currMax = Math.min(currMax, highs[j]);
            }
            if(possible) {
                out.println("YES");
            }else {
                out.println("NO");
            }
        }
    }
}
