package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ASumOfOddIntegers {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            long n = in.nextInt();
            long k = in.nextInt();
            String possible = "NO";
            if ((n % 2 == 0 && k % 2 == 0) || (n % 2 == 1 && k % 2 == 1)) {
                if (k * k <= n) {
                    possible = "YES";
                } else {
                    possible = "NO";
                }
            } else {
                //noop
            }
            out.println(possible);
        }
    }
}
