package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AStationAndBus {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String str = in.nextLine();
        if(str.indexOf("A") == -1 || str.indexOf("B") == -1){
            out.println("No");
        }else {
            out.println("Yes");
        }
    }
}
