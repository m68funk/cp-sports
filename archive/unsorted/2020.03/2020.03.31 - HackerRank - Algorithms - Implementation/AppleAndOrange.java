package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AppleAndOrange {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int s = in.nextInt();
        int t = in.nextInt();

        int a = in.nextInt();
        int b = in.nextInt();

        int m = in.nextInt();
        int n = in.nextInt();


        int aCount = 0;
        for (int i = 0; i < m; i++) {
            int ad = a + in.nextInt();
            if (ad >= s && ad <= t) aCount++;
        }
        out.println(aCount);

        int bCount = 0;
        for (int i = 0; i < n; i++) {
            int bd = b + in.nextInt();
            if (bd >= s && bd <= t) bCount++;
        }
        out.println(bCount);
    }
}
