package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BMakeProductEqualOne {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        long cost = 0;
        int nC = 0;
        int zC = 0;
        for (int i = 0; i < n; i++) {
            long a = in.nextInt();
            cost = cost + Math.abs(1 - Math.abs(a));
            nC = a < 0 ? ++nC : nC;
            zC = a == 0 ? ++zC : zC;
        }
        boolean addTwo = false;
        if (nC % 2 == 1 && zC == 0) {
            out.println(cost + 2);
        } else {
            out.println(cost);
        }
    }
}
