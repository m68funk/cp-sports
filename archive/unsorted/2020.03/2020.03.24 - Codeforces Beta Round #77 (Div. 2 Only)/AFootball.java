package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AFootball {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String positions = in.nextLine();
        int c0 = 0;
        int c1 = 0;
        for (int i = 0; i < positions.length(); i++) {
            switch (positions.charAt(i)) {
                case '0':
                    c1 = 0;
                    c0++;
                    break;
                case '1':
                    c0 = 0;
                    c1++;
                    break;
            }
            if (c1 >= 7 || c0 >= 7) {
                out.println("YES");
                return;
            }
        }
        out.println("NO");
    }
}
