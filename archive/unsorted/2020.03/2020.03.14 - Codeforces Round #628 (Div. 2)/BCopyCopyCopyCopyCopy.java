package main;

import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class BCopyCopyCopyCopyCopy {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            Set set = new HashSet<Integer>();
            int n = in.nextInt();
            for (int j = 0; j < n; j++) {
                set.add(in.nextInt());
            }
            out.println(set.size());
        }
    }
}
