package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AEhAbAnDGCd {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int x = in.nextInt();

            for (int j = 1; j <= x / 2; j++) {
                int gcd = gcd(j, x - j);
                int lcm = lcm(j, x - j);
                if (gcd + lcm == x) {
                    out.println(j + " " + (x - j));
                    break;
                }
            }

        }
    }

    public static int gcd(int n1, int n2) {
        if (n2 == 0) {
            return n1;
        }
        return gcd(n2, n1 % n2);
    }

    public static int lcm(int number1, int number2) {
        if (number1 == 0 || number2 == 0)
            return 0;
        else {
            int gcd = gcd(number1, number2);
            return Math.abs(number1 * number2) / gcd;
        }
    }
}
