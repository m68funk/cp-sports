package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BirthdayCakeCandles {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int max = 0;
        int maxCounter = 0;
        for (int i = 0; i < n; i++) {
            int ar = in.nextInt();
            if (ar > max) {
                max = ar;
                maxCounter = 1;
            } else if (ar == max) {
                maxCounter++;
            }
        }
        out.println(maxCounter);
    }
}
