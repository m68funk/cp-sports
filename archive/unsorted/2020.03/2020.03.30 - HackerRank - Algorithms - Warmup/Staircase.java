package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class Staircase {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <n ; j++) {
                if (i + j >= n -1) {
                    out.print("#");
                } else {
                    out.print(" ");
                }
            }
            out.println();
        }
    }
}
