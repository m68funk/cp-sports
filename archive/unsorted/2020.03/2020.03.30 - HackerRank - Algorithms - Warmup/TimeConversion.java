package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class TimeConversion {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String time = in.nextLine();
        boolean isPM = time.substring(8).equals("PM");
        boolean is12 = time.substring(0, 2).equals("12");
        if (isPM && is12) {
            time = 12 + time.substring(2, 8);
        } else if (isPM && !is12) {
            time = (Integer.parseInt(time.substring(0, 2)) + 12) % 24 + time.substring(2, 8);
        } else if (!isPM && is12) {
            time = "00" + time.substring(2, 8);
        } else if (!isPM && !is12) {
            time = time.substring(0, 8);
        }
        out.println(time);
    }
}
