package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BCormenTheBestFriendOfAMan {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int k = in.nextInt();

        int[] a = new int[n + 1];
        int[] b = new int[n + 1];

        a[0] = k;
        b[0] = k;
        int s = 0;
        for (int i = 1; i <= n; i++) {
            a[i] = in.nextInt();
            b[i] = Math.max(k - b[i - 1], a[i]);
            s = s + b[i] - a[i];
        }
        out.println(s);
        for (int i = 1; i <= n; i++) {
            out.print(b[i] + " ");
        }

    }
}
