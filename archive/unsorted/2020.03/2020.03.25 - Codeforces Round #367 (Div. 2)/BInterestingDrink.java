package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BInterestingDrink {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int[] cs = new int[100001];
        for (int i = 0; i < n; i++) {
            cs[in.nextInt()]++;
        }
        for (int i = 1; i < 100001; i++) {
            cs[i] = cs[i] + cs[i - 1];
        }
//        int max = cs[n - 1];
        int q = in.nextInt();
        for (int i = 0; i < q; i++) {
            int money = in.nextInt();
            if (money > 100000) {
                out.println(n);
            } else {
                out.println(cs[money]);
            }
        }
    }
}
