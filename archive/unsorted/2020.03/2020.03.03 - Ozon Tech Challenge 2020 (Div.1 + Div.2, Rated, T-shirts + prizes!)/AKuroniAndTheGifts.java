package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class AKuroniAndTheGifts {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        for (int j = 0; j < n; j++) {
            int t = in.nextInt();

            int[] neck = new int[t];
            int[] arm = new int[t];

            for (int i = 0; i < t; i++) {
                neck[i] = in.nextInt();
            }
            Arrays.sort(neck);
            for (int i = 0; i < t; i++) {
                out.write(neck[i] + " ");
            }
            neck = null;
            out.println();
            for (int i = 0; i < t; i++) {
                arm[i] = in.nextInt();
            }
            Arrays.sort(arm);
            for (int i = 0; i < t; i++) {
                out.write(arm[i] + " ");
            }
            out.println();
        }
        out.flush();
    }
}
