package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class CKuroniAndImpossibleCalculation {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int modulo = in.nextInt();

        long res = 1;

        int[] as = new int[n];

        for (int i = 0; i < n; i++) {
            as[i] = in.nextInt();
        }

        int[] counts = new int[modulo];

        for (int i = 0; i < n; i++) {
            counts[as[i] % modulo]++;
        }
        for (int i = 0; i < modulo; i++) {
            if (counts[i] >= 2) {
                res = 0;
                break;
            }
        }
        if (res == 0) {
            out.println(0);
        } else {
            for (int i = 0; i < n - 1; i++) {
                for (int j = i + 1; j < n; j++) {
                    res = (res * (Math.abs(as[i] - as[j]) % modulo) % modulo);
                }
            }
            out.println(res);
        }
    }
}
