package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BKuroniAndSimpleStrings {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        char[] s = in.nextLine().toCharArray();
        String lefts = "";
        String rights = "";
        int l = 0;
        int r = s.length;
        int count = 0;
        while (l < r) {
            if (s[l] == ')') {
                l++;
                continue;
            } else if (s[r - 1] == '(') {
                r--;
                continue;
            } else {
                lefts = lefts + " " + (l + 1);
                rights = r + " " + rights;
                count++;
                count++;
                l++;
                r--;
            }
        }

        if (count == 0) {
            out.println(0);
        } else {
            out.println(1);
            out.println(count);
            out.println(lefts.trim() + " " + rights.trim());
        }
    }
}
