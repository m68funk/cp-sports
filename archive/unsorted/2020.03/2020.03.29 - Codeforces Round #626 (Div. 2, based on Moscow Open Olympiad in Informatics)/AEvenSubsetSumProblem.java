package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AEvenSubsetSumProblem {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int count = 0;
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
                if (a[j] % 2 == 1) {
                    count++;
                }
            }
            boolean dropOne = count % 2 == 1;
            if (dropOne && n == 1) {
                out.println("-1");
            } else {
                if(dropOne){
                    out.println(n -1);
                } else {
                    out.println(n);
                }
                for (int j = 0; j < n; j++) {
                    if (a[j] % 2 == 1 && dropOne) {
                        dropOne = false;
                    } else {
                        out.print(j + 1 + " ");
                    }
                }
                out.println();
            }
        }
    }
}
