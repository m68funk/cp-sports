package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ADuplexPrinting {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        out.println(n / 2 + n % 2);
    }
}
