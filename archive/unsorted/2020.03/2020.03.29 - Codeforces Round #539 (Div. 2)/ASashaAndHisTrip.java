package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ASashaAndHisTrip {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int v = in.nextInt();

        int tail = Math.min(n - 1, v);

        int stops = Math.max(0, n - v);

        int cost = stops == 0 ? tail : stops * (stops + 1) / 2 + v - 1;

        out.println(cost);
    }
}
