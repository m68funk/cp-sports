package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AHitachiString {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String hi = in.nextLine();
        boolean itIs = true;
        if (hi.length() % 2 == 1) {
            itIs = false;
        }
        for (int i = 0; i < hi.length() / 2; i++) {
            if (!hi.substring(i * 2, i * 2 + 2).equals("hi")) {
                itIs = false;
                break;
            }
        }
        if(itIs){
            out.println("Yes");
        } else {
            out.println("No");
        }
    }
}
