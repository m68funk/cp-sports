package main;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class BWeirdSort {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int nA = in.nextInt();
            int nP = in.nextInt();
            int[] arr = new int[nA];
            for (int j = 0; j < arr.length; j++) {
                arr[j] = in.nextInt();
            }
            boolean[] per = new boolean[nA];
            Arrays.fill(per, false);
            for (int j = 0; j < nP; j++) {
                per[in.nextInt() - 1] = true;
            }

            for (int n = arr.length; n > 1; --n) {
                for (int j = 0; j < n - 1; ++j) {
                    if (arr[j] > arr[j + 1]) {
                        if (per[j]) {
                            int temp = arr[j];
                            arr[j] = arr[j + 1];
                            arr[j + 1] = temp;
                        }

                    }
                }

            }

            boolean doable = true;
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    doable = false;
                    break;
                }
            }
            if (doable) {
                out.println("YES");
            } else {
                out.println("NO");
            }
        }
    }

    private int find(int val, int i, int[] sorted) {
        for (int j = i; j < sorted.length; j++) {
            if (sorted[j] == val) return j;
        }
        return i;
    }
}
