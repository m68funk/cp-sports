package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AAddOddOrSubtractEven {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            if (a == b) {
                out.println(0);
            } else if ((a > b && (a - b) % 2 == 0) || (a < b && (a - b) % 2 != 0)) {
                out.println(1);
            } else {
                out.println(2);
            }
        }
    }
}
