package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AHexagonalNumbers {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        out.println((2*n -1)*n);
    }
}
