package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.io.PrintWriter;

public class AFastFoodRestaurant {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int[] dd = new int[3];
            dd[0] = in.nextInt();
            dd[1] = in.nextInt();
            dd[2] = in.nextInt();
            int res = 0;
            for (int j = 0; j < 3; j++) {
                if (dd[j] > 0) {
                    dd[j]--;
                    res++;
                }
            }
            Arrays.sort(dd);
            if (dd[1] > 0 && dd[2] > 0) {
                dd[1]--;
                dd[2]--;
                res++;
            }
            if (dd[0] > 0 && dd[2] > 0) {
                dd[0]--;
                dd[2]--;
                res++;
            }
            if (dd[0] > 0 && dd[1] > 0) {
                dd[0]--;
                dd[1]--;
                res++;
            }
            if (dd[0] > 0 && dd[1] > 0 && dd[2] > 0) {
                res++;
            }
            out.println(res);
        }
    }
}
