package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AKefaAndFirstSteps {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        int[] a = new int[t];
        for (int i = 0; i < a.length; i++) {
            a[i] = in.nextInt();
        }
        int res = 1;
        int curr = 1;
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) {
                curr = 1;
            } else {
                curr++;
                res = Math.max(res, curr);
            }
        }
        out.println(res);
    }
}
