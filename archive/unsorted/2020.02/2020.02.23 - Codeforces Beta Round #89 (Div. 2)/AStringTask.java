package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AStringTask {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        char[] txt = in.next().toCharArray();
        StringBuilder resp = new StringBuilder();
        String vls = "AOYEUIaoyeui";
        for (int i = 0; i < txt.length; i++) {
            if (vls.indexOf(txt[i]) != -1) {
                //do nothing
            } else {
                resp.append(".").append(txt[i]);
            }
        }
        out.println(resp.toString().toLowerCase());
    }
}
