package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ALuckyDivision {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int number = in.nextInt();
        if (number % 4 == 0) {
            out.println("YES");
        } else if (number % 7 == 0) {
            out.println("YES");
        } else if (number % 44 == 0) {
            out.println("YES");
        } else if (number % 47 == 0) {
            out.println("YES");
        } else if (number % 74 == 0) {
            out.println("YES");
        } else if (number % 77 == 0) {
            out.println("YES");
        } else if (number > 100 && isLucky(number)) {
            out.println("YES");
        } else {
            out.println("NO");
        }
    }

    private boolean isLucky(int number) {
        String n = Integer.toString(number);
        char[] noLuck = {'0','1','2','3','5','6','8','9'};
        for (int i = 0; i < noLuck.length; i++) {
            if(n.indexOf(noLuck[i]) != -1) return false;
        }
        return true;
    }
}
