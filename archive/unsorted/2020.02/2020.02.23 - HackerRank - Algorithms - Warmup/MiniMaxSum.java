package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class MiniMaxSum {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        long[] arr = new long[5];
        for (int i = 0; i < 5; i++) {
            arr[i] = in.nextLong();
        }
        Arrays.sort(arr);
        long min =0;
        for (int i = 0; i < 4; i++) {
            min = min + arr[i];
        }
        long max =0;
        for (int i = 1; i < 5; i++) {
            max = max + arr[i];
        }
        out.println(min + " " + max);
    }
}
