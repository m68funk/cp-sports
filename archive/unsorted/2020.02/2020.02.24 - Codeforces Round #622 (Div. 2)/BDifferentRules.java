package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BDifferentRules {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();

            int min = Math.max(1, Math.min(n, x + y - n + 1));
            int max = Math.min(n, ((x + y) - 1));
            out.println(min + " " + max);
        }
    }
}
