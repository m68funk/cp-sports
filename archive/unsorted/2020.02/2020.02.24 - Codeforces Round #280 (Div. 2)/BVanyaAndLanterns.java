package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BVanyaAndLanterns {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int l = in.nextInt();

        int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        Arrays.sort(a);
        double max = 0d;
        for (int i = 0; i < a.length - 1; i++) {
            max = Math.max(max, (a[i +1] - a[i])/2d);
        }
        max = Math.max(max, a[0]);
        max = Math.max(max, l - a[n-1]);
        out.println(String.format("%.10f", max));
    }
}
