package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class AAnuHasAFunction {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        long[] arr = new long[n];
        long[] pref = new long[n];
        long[] suffix = new long[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextLong();
        }

        pref[0] = arr[0];
        for (int i = 1; i < n; i++) {
            pref[i] = pref[i - 1] | arr[i];
        }

        suffix[n - 1] = arr[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            suffix[i] = suffix[i + 1] | arr[i];
        }

        long max = 0;
        int index = 0;

        for (int i = 1; i < n - 1; i++) {
            long cand = (arr[i] & ~(pref[i - 1] | suffix[i + 1]));
            if (cand > max) {
                max = cand;
                index = i;
            }
        }
        if (n > 1) {
            if ((arr[0] & ~suffix[1]) > max) {
                max = (arr[0] & ~suffix[1]);
                index = 0;
            }
            if ((arr[n - 1] & ~pref[n - 2]) > max) {
//            max = (arr[n-1] & ~pref[n-1]);
                index = n - 1;
            }
        }

        out.write(arr[index] + " ");
        for (int i = arr.length - 1; i >= 0; i--) {
            if (i != index) {
                out.write(arr[i] + " ");
            }
        }
    }
}
