package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.io.PrintWriter;

public class ATwins {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        Integer[] cs = new Integer[t];
        int sum = 0;
        for (int i = 0; i < cs.length; i++) {
            cs[i] = in.nextInt();
            sum = sum + cs[i];
        }
        Arrays.sort(cs, Comparator.reverseOrder());
        int tmp = 0;
        for (int i = 0; i < cs.length; i++) {
            tmp = tmp + cs[i];
            if (2 * tmp > sum) {
                out.println(i + 1);
                break;
            }
        }
    }
}
