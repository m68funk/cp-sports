package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ACutRibbon {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();

        int max = 0;

        for (int i = 0; i <= n / a; i++) {
            for (int j = 0; j <= (n - i * a) / b; j++) {
                int cand = n - i * a - j * b;
                if (cand % c == 0) {
                    int tmp = i + j + cand / c;
                    max = tmp > max ? tmp : max;
                }
            }
        }
        out.println(max);
    }
}
