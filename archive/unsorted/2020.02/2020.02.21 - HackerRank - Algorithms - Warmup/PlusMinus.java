package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class PlusMinus {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        int[] nums = new int[]{0, 0, 0};
        for (int i = 0; i < t; i++) {
            int next = in.nextInt();
            if (next > 0) {
                nums[0]++;
            } else if (next < 0) {
                nums[1]++;
            } else if (next == 0) {
                nums[2]++;
            }
        }
        for (int i = 0; i < 3; i++) {
            out.println(nums[i] / (double) t);
        }
    }
}
