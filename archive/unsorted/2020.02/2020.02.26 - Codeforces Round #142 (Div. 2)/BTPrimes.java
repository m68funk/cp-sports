package main;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class BTPrimes {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        final int MAX = 1000000;

        int t = in.nextInt();
        boolean[] sieb = new boolean[MAX];
        Arrays.fill(sieb, true);
        for (int i = 2; i <= 1000; i++) {
            if (sieb[i-1]) {
                for (int j = 2; (i) * j <= MAX; j++) {
                    sieb[((i) * j) - 1] = false;
                }
            }
        }
        Set<Long> primeSqares = new HashSet<>();
        for (int i = 1; i < sieb.length; i++) {
            if (sieb[i]) {
                primeSqares.add((long) (i + 1) * (long) (i + 1));
            }
        }
        for (int i = 0; i < t; i++) {
            long cand = in.nextLong();
            if (primeSqares.contains(cand)) {
                out.println("YES");
            } else {
                out.println("NO");
            }
            ;
        }
    }
}
