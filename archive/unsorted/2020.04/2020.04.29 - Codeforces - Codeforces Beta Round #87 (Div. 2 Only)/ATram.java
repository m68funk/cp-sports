package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ATram {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();

        int max = 0;
        int curr = 0;
        for (int i = 0; i < n; i++) {
            curr = curr - in.readInt();
            curr = curr + in.readInt();
            max = Math.max(curr, max);
        }
        out.println(max);
    }
}
