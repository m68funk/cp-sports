package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int x = 0;
        int y = 0;
        int z = 0;
        for (int i = 0; i < n; i++) {
            x = x + in.readInt();
            y = y + in.readInt();
            z = z + in.readInt();
        }
        if(x == 0 && y == 0 && z == 0){
            out.println("YES");
        } else {
            out.println("NO");
        }
    }
}
