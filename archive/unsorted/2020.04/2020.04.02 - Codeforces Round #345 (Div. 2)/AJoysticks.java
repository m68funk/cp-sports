package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AJoysticks {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int left = in.nextInt();
        int right = in.nextInt();

        int time = 0;

        while (left * right > 1){
            time++;

            if(left < right){
                left++;
                right--;
                right--;
            } else {
                left--;
                left--;
                right++;
            }
        }
        out.println(time);
    }
}
