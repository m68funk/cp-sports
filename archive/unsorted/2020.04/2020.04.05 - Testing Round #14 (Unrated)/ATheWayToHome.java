package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ATheWayToHome {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int d = in.nextInt();
        in.nextLine();
        String lillies = in.nextLine();

        int start = 0;
        int jumpCount = 0;

        do {
            String subl = lillies.substring(start, Math.min(n, start + d + 1));
            int jump = subl.lastIndexOf('1');
            if (jump < 1) {
                jumpCount = -1;
                break;
            }
            start = start + jump;
            jumpCount++;
        } while (start < n - 1);

        out.println(jumpCount);


    }
}
