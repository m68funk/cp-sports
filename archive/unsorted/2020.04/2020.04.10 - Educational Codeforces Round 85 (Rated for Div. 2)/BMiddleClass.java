package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.io.PrintWriter;

public class BMiddleClass {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            long x = in.nextLong();
            Long[] a = new Long[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextLong();
            }
            Arrays.sort(a, Collections.reverseOrder());
            for (int j = 1; j < n; j++) {
                a[j] = a[j - 1] + a[j];
            }
            int res = n;
            for (int j = 0; j < n; j++) {
                if (a[j] < (j + 1) * x) {
                    res = j;
                    break;
                }
            }
            out.println(res);
        }
    }
}
