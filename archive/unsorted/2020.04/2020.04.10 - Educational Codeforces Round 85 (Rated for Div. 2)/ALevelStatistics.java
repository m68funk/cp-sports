package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ALevelStatistics {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int p = 0;
            int c = 0;
            boolean possible = true;
            for (int j = 0; j < n; j++) {
                int p_curr = in.nextInt();
                int c_curr = in.nextInt();
                if (possible) {
                    if (p > p_curr) {
                        possible = false;
                    }
                    if (c > c_curr) {
                        possible = false;
                    }
                    if (c_curr - c > p_curr - p) {
                        possible = false;
                    }
                    p = p_curr;
                    c = c_curr;
                }
            }
            out.println(possible ? "YES" : "NO");
        }
    }
}
