package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ASantaClausAndCandies {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();

        int count = 0;
        int[] kids = new int[500];

        while (n > 0) {
            if (2 * (count + 1) + 1 <= n) {
                kids[count] = count + 1;
                n = n - count - 1;
            } else {
                kids[count] = n;
                n = 0;
            }
            count++;
        }
        out.println(count);
        for (int i = 0; i < count; i++) {
            out.print(kids[i] + " ");
        }
    }
}
