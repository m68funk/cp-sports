package main;

import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class ADreamoonAndRankingCollection {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int x = in.nextInt();

            Set<Integer> set = new HashSet<>();
            for (int j = 0; j < n; j++) {
                set.add(in.nextInt());
            }

            int res = 0;

            do {
                res++;
                if (!set.contains(res)) {
                    x--;
                }
            }
            while (x > 0 || set.contains(res + 1));
            out.println(res);
        }
    }
}
