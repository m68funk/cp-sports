package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int outer = 0;
        for (int i = 0; i < n; i++) {
            int inner = 0;
            for (int j = 0; j < 3; j++) {
                inner = inner + in.readInt();
            }
            outer = inner > 1 ? outer + 1 : outer;
        }
        out.println(outer);
    }
}
