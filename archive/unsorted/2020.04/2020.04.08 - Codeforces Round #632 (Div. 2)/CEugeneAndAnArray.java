package main;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.PrintWriter;

public class CEugeneAndAnArray {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        long result = 0;
        long sum = 0;
        int st = 1;
        Map<Long, Integer> frec = new HashMap<>();
        frec.put(0l, 1);

        for (int i = 1; i <= n; i++) {
            int val = in.nextInt();

            sum = sum + val;

            st = Math.max(st, frec.getOrDefault(sum, 0) + 1);

            frec.put(sum, i + 1);

            result = result + (i + 1 - st);
        }

        out.println(result);
    }
}
