package main;

import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class BKindAnton {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
            }
            int[] b = new int[n];
            for (int j = 0; j < n; j++) {
                b[j] = in.nextInt();
            }
            boolean possible = true;
            Set<Integer> ops = new HashSet<>();
            ops.add(0);
            for (int j = 0; j < n; j++) {
                if (!ops.contains(Integer.compare(b[j], a[j]))) {
                    possible = false;
                    break;
                }
                ops.add(a[j]);
            }
            out.println(possible ? "YES" : "NO");
        }
    }
}
