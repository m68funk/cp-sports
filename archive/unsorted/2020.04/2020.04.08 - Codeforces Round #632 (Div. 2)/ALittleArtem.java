package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ALittleArtem {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int m = in.nextInt();
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < m; k++) {
                    if (j == 0 && k == 0) {
                        out.print("W");
                    } else {
                        out.print("B");
                    }
                }
                out.println();
            }
        }
        out.println();
    }
}
