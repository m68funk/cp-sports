package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskC {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            long prev = in.readLong();
            long prevMax = prev;
            long carry = 0;
            for (int j = 1; j < n; j++) {
                long curr = in.readLong();
                boolean isChange = prev * curr < 0;
                if (j < n - 1) {
                    if (isChange) {
                        carry = carry + prevMax;
                        prev = curr;
                        prevMax = curr;
                    } else {
                        prev = curr;
                        prevMax = Math.max(prevMax, curr);
                    }
                } else {
                    if (isChange) {
                        carry = carry + prevMax + curr;
                    } else {
                        prevMax = Math.max(prevMax, curr);
                        carry = carry + prevMax;
                    }
                }
            }
            if (n == 1) {
                out.println(prev);
            } else {
                out.println(carry);
            }
        }
    }
}
