package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            long n = in.readLong();
            int k = 2;
            while (n % (((long) Math.pow(2, k)) - 1) != 0) {
                k++;
            }
            out.println(n / ((long) Math.pow(2, k) - 1));
        }
    }
}
