package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskD {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int[] a = new int[n];
            int k = in.readInt();
            for (int j = 0; j < n; j++) {
                a[j] = in.readInt();
            }
            int[] ks = new int[2 * k + 1];
            for (int j = 0; j < n / 2; j++) {
                int curr = a[j] + a[n - j - 1];
                ks[curr] = ks[curr] + 1;
            }
            int indexOfMax = 0;
            for (int j = 0; j <= 2 * k; j++) {
                if (ks[j] > ks[indexOfMax]) {
                    indexOfMax = j;
                }
            }
            out.println(n / 2 - ks[indexOfMax]);
        }
    }
}
