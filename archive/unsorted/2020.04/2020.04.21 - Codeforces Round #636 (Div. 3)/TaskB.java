package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            if (n % 4 != 0) {
                out.println("NO");
            } else {
                out.println("YES");
                int[] res = new int[n];
                int reminder = 0;
                for (int j = 0; j < n / 2; j++) {
                    res[j] = 2 * (j + 1);
                    reminder = reminder + res[j];
                }
                for (int j = n / 2; j < n - 1; j++) {
                    res[j] = 2 * (j - n / 2) + 1;
                    reminder = reminder - res[j];
                }
                res[n - 1] = reminder;
                for (int j = 0; j < n; j++) {
                    out.print(res[j] + " ");
                }
                out.println();
            }
        }
    }
}
