package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            int door = in.readInt();
            long[] a = new long[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.readLong();
            }

            int[] pC = new int[n];
            for (int j = 1; j < n - 1; j++) {
                if (a[j - 1] < a[j] && a[j] > a[j + 1]) {
                    pC[j] = pC[j - 1] + 1;
                } else {
                    pC[j] = pC[j - 1];
                }
            }
            pC[n - 1] = pC[n - 2];

            int pMax = 0;
            int pIndex = 0;

            for (int j = 0; j < n - door + 1; j++) {
                int lpC = pC[j + door - 2] - pC[j];
                if (lpC > pMax) {
                    pMax = lpC;
                    pIndex = j;
                }
            }

            out.println(pMax + 1 + " " + (pIndex + 1));
        }
    }
}
