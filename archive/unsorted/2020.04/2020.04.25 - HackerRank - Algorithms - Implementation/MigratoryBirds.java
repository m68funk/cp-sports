package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class MigratoryBirds {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] types = new int[5];
        for (int i = 0; i < n; i++) {
            types[in.readInt() - 1]++;
        }
        int p = 0;
        for (int i = 0; i < 5; i++) {
            if (types[i] > types[p]) {
                p = i;
            }
        }
        out.println(p + 1);
    }
}
