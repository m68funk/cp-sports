package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class DivisibleSumPairs {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int k = in.readInt();
        int[] ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.readInt();
        }
        int counter = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i+1; j < n ; j++) {
                if((ar[i] + ar[j]) %k ==0){
                    counter++;
                }
            }
        }
        out.println(counter);
    }
}
