package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class BreakingTheRecords {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int first = in.readInt();
        int min = first;
        int minCounter = 0;
        int max = first;
        int maxCounter = 0;
        for (int i = 1; i < n; i++) {
            int current = in.readInt();
            if(current < min){
                minCounter++;
                min = current;
            }
            if(current > max){
                maxCounter++;
                max = current;
            }
        }
        out.println(maxCounter + " " + minCounter);
    }
}
