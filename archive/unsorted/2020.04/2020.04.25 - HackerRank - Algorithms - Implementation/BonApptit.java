package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BonApptit {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int k = in.readInt();
        int[] bill = new int[n];
        for (int i = 0; i < n; i++) {
            bill[i] = in.readInt();
        }
        int charged = in.readInt();
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum = sum + bill[i];
        }
        sum = sum - bill[k];
        if (sum / 2 < charged) {
            out.println(charged - (sum / 2));
        } else {
            out.println("Bon Appetit");
        }
    }
}
