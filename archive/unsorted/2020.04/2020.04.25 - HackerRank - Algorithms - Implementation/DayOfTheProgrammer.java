package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class DayOfTheProgrammer {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int y = in.readInt();
        int dd = 13;
        int mm = 9;
        int yy = y;

        if (yy < 1918) {
            if (yy % 4 == 0) {
                dd = 12;
            }
        } else if (yy == 1918) {
            dd = 26;
        } else {
            if (yy % 400 == 0) {
                dd = 12;
            } else if (yy % 4 == 0 && yy % 100 != 0) {
                dd = 12;
            }
        }
        out.println(dd + ".0" + mm + "." + yy);
    }
}
