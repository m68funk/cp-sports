package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BirthdayChocolate {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] s = new int[n+1];
        for (int i = 1; i <= n; i++) {
            s[i] = in.readInt() + s[i - 1];
        }
        int d = in.readInt();
        int m = in.readInt();
        int counter = 0;
        for (int i = 0; i <= n - m; i++) {
            if(s[i + m] - s[i] == d){
                counter++;
            }
        }
        out.println(counter);
    }
}
