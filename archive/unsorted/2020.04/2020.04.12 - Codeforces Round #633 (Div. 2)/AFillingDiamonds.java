package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AFillingDiamonds {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            long res = 0;

            switch (n) {
                case 1:
                    res = 1;
                    break;
                case 2:
                    res = 2;
                    break;
                default:
                    res = n;
            }

            out.println(res);
        }
    }
}
