package main;

import java.util.Arrays;
import java.util.Scanner;
import java.io.PrintWriter;

public class BSortedAdjacentDifferences {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int[] a = new int[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
            }
            Arrays.sort(a);
            if (n % 2 == 1) {
                out.print(a[n / 2] + " ");
                for (int j = 0; j < n / 2; j++) {
                    out.print(a[n / 2 - j - 1] + " ");
                    out.print(a[n / 2 + j + 1] + " ");
                }
            } else {
                for (int j = 0; j < n / 2; j++) {
                    out.print(a[n / 2 - j - 1] + " ");
                    out.print(a[n / 2 + j] + " ");
                }
            }
            out.println();
        }
    }
}
