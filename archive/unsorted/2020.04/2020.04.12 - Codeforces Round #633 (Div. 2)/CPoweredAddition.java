package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class CPoweredAddition {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int currMax = in.nextInt();
            int currMaxDecrease = 0;
            for (int j = 1; j < n; j++) {
                int curr = in.nextInt();
                if (curr < currMax) {
                    if (currMax - curr > currMaxDecrease) {
                        currMaxDecrease = currMax - curr;
                    }
                } else {
                    currMax = curr;
                }
            }
            int res = currMaxDecrease == 0 ? 0 : (int) (Math.log(currMaxDecrease) / Math.log(2)) + 1;
            out.println(res);
        }
    }
}
