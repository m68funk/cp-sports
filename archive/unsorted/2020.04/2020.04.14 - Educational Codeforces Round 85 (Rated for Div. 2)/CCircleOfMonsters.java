package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class CCircleOfMonsters {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        int l = 300001;
        long[] a = new long[l];
        long[] b = new long[l];
        for (int i = 0; i < t; i++) {
            int n = in.readInt();
            for (int j = 0; j < n; j++) {
                a[j] = in.readLong();
                b[j] = in.readLong();
            }
            long ans = 0;
            long mn = Long.MAX_VALUE;
            for (int j = 0; j < n; j++) {
                int ni = (j + 1) % n;
                long val = Math.min(a[ni], b[j]);
                ans += a[ni] - val;
                mn = Math.min(mn, val);
            }
            ans += mn;
            out.println(ans);
        }
    }

}
