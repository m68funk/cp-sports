package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AStonesOnTheTable {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        String s = in.readToken();

        int ct = 1;

        for (int i = 1; i < n; i++) {
            if (s.charAt(i - 1) != s.charAt(i)) {
                ct++;
            }
        }
        out.println(n - ct);
    }
}
