package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Scanner;

public class JavaStdinAndStdoutII {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int i = in.readInt();
        double d = in.readDouble();
        String s = in.readToken();
        out.println("String: " + s);
        out.println("Double: " + d);
        out.println("Int: " + i);
    }
}
