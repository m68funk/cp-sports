package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class JavaIfElse {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();

        if(n % 2 ==1){
            out.println("Weird");
        } else {
            if(2 <= n && n <= 5){
                out.println("Not Weird");
            } else if(6 <= n && n <= 20){
                out.println("Weird");
            } else {
                out.println("Not Weird");
            }
        }
    }
}
