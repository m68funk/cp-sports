package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BFence {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int k = in.nextInt();

        int[] fence = new int[n];
        for (int i = 0; i < n; i++) {
            fence[i] = in.nextInt();
        }
        int[] sums = new int[n - k + 1];
        for (int i = n - k; i < n; i++) {
            sums[n - k] = sums[n - k] + fence[i];
        }
        for (int i = n - k - 1; i >= 0; i--) {
            sums[i] = sums[i + 1] + fence[i] - fence[i + k];
        }
        int index = 0;
        for (int i = 0; i < sums.length; i++) {
            index = sums[index] < sums[i] ? index : i;
        }
        out.println(index + 1);
    }
}
