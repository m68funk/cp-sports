package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BIlyaAndQueries {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        char[] s = in.nextLine().toCharArray();
        int length = s.length;
        int[] seq = new int[length - 1];
        for (int i = 0; i < length - 1; i++) {
            seq[i] = s[i] == s[i + 1] ? 1 : 0;
        }

        int[] si = new int[length];
        for (int i = 1; i < length; i++) {
            si[i] = si[i - 1] + seq[i - 1];
        }

        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int l = in.nextInt();
            int r = in.nextInt();
            out.println(si[r - 1] - si[l - 1]);
        }
    }
}
