package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class JavaLoopsI {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        for (int i = 1; i <= 10; i++) {
            out.println(n + " x " + i + " = " + n * i);
        }
    }
}
