package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class JavaLoopsII {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int q = in.readInt();
        for (int i = 0; i < q; i++) {
            long a = in.readLong();
            long b = in.readLong();
            long n = in.readLong();

            for (int j = 0; j < n; j++) {
                long sum = a;
                for (int k = 0; k <= j; k++) {
                    sum = sum + ((long) Math.pow(2, k) * b);
                }
                out.print( sum + " ");
            }
            out.println();
            Byte.MIN_VALUE
        }
    }
}
