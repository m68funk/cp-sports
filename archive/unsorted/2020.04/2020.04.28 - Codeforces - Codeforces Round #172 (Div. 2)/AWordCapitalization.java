package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AWordCapitalization {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();
        out.println(s.substring(0, 1).toUpperCase() + s.substring(1, s.length()));
    }
}
