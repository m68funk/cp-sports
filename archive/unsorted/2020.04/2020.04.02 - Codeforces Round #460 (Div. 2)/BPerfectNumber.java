package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BPerfectNumber {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        long[] ks = new long[10000];
        int k = 0;
        int i = 19;
        while (k < 10000) {
            if (sumDigits(i) == 10) {
                ks[k] = i;
                k++;
            }
            i++;
        }
        k = in.nextInt();
        out.println(ks[k - 1]);
    }

    public static long sumDigits(long i) {
        return i == 0 ? 0 : i % 10 + sumDigits(i / 10);
    }
}
