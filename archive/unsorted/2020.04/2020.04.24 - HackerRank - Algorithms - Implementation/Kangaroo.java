package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class Kangaroo {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int x1 = in.readInt();
        int v1 = in.readInt();
        int x2 = in.readInt();
        int v2 = in.readInt();

        if (v2 == v1) {
            out.println((x1 == x2) ? "YES" : "NO");
        } else {
            int div = (x1 - x2) / (v2 - v1);
            int rem = (x1 - x2) % (v2 - v1);

            String res;
            if (div > 0 && rem == 0) {
                res = "YES";
            } else {
                res = "NO";
            }
            out.println(res);
        }
    }
}
