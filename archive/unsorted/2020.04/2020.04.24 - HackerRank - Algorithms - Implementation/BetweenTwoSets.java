package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class BetweenTwoSets {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int m = in.readInt();

        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.readInt();
        }
        int[] b = new int[m];
        for (int i = 0; i < m; i++) {
            b[i] = in.readInt();
        }
        int count = 0;
        for (int i = 1; i <= 100; i++) {
            boolean cand = true;
            for (int j = 0; j < n; j++) {
                if(i % a[j] != 0){
                    cand = false;
                    break;
                }
            }
            if(cand){
                for (int j = 0; j < m; j++) {
                    if(b[j] % i != 0){
                        cand = false;
                        break;
                    }
                }
            }
            if(cand){
                count++;
            }
        }
        out.println(count);
    }
}
