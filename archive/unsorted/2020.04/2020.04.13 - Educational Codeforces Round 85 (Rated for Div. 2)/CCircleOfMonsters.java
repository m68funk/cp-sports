package main;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.PrintWriter;

public class CCircleOfMonsters {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        int l = 300001;
        long[] a = new long[l];
        long[] b = new long[l];
        String str;
        int index;
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            in.nextLine();
            for (int j = 0; j < n; j++) {
                str = in.nextLine();
                index = str.indexOf(" ");
                a[j] = Long.parseLong(str.substring(0,index));
                b[j] = Long.parseLong(str.substring(index + 1));
            }
            long ans = 0;
            long mn = Long.MAX_VALUE;
            for (int j = 0; j < n; j++) {
                int ni = (j + 1) % n;
                long val = Math.min(a[ni], b[j]);
                ans += a[ni] - val;
                mn = Math.min(mn, val);
            }
            ans += mn;
            out.print(ans +"\n");
        }
    }
}
