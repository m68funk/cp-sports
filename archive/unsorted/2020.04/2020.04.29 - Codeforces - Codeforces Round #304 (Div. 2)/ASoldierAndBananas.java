package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class ASoldierAndBananas {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int k = in.readInt();
        int n = in.readInt();
        int w = in.readInt();


        int res = k * w * (w +1) / 2 - n;
        res = Math.max(0, res);
        out.println(res);
    }
}
