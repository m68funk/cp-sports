package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class APolandBallAndHypothesis {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int z = 100000;
        boolean prime[] = new boolean[z + 1];
        for (int i = 0; i < z; i++)
            prime[i] = true;

        for (int p = 2; p * p <= z; p++) {
            // If prime[p] is not changed, then it is a prime
            if (prime[p] == true) {
                // Update all multiples of p
                for (int i = p * 2; i <= z; i += p)
                    prime[i] = false;
            }
        }

        int n = in.nextInt();
        int m = 1;
        while (prime[n * m + 1]) {
            m++;
        }
        out.println(m);
    }

    boolean isPrime(int n) {
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}
