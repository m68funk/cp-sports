package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class ABoyOrGirl {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();

        Set<Character> sc = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            sc.add(s.charAt(i));
        }
        out.println(sc.size() % 2 == 0 ? "CHAT WITH HER!" : "IGNORE HIM!");
    }
}
