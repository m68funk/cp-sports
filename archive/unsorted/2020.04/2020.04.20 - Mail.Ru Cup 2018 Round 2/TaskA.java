package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int s = in.readInt();

        int[] ft = new int[n];
        for (int i = 0; i < n; i++) {
            ft[i] = in.readInt();
        }
        int[] st = new int[n];
        for (int i = 0; i < n; i++) {
            st[i] = in.readInt();
        }
        if (ft[0] == 0) {
            out.println("NO");
            return;
        }
        if (ft[s - 1] == 1) {
            out.println("YES");
            return;
        }
        if (ft[s - 1] == 0 && st[s - 1] == 0) {
            out.println("NO");
            return;
        }
        for (int i = s; i < n; i++) {
            if (ft[i] == 1 && st[i] == 1) {
                out.println("YES");
                return;
            }
        }
        out.println("NO");
        return;
    }
}
