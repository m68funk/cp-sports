package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int qx = in.readInt();
        int qy = in.readInt();
        int kx = in.readInt();
        int ky = in.readInt();
        int tx = in.readInt();
        int ty = in.readInt();

        kx -= qx;
        ky -= qy;

        tx -= qx;
        ty -= qy;

        if (kx * tx < 0 ||
                ky * ty < 0) {
            out.println("NO");
        } else {
            out.println("YES");
        }
    }
}