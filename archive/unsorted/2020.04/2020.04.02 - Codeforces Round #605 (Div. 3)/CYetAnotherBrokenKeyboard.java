package main;

import java.util.HashSet;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.Set;

public class CYetAnotherBrokenKeyboard {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int n = in.nextInt();
        int k = in.nextInt();
        String s = in.next();
        Set<String> c = new HashSet();
        for (int i = 0; i < k; i++) {
            c.add(in.next());
        }

        int[] contains = new int[n];
        for (int i = 0; i < n; i++) {
            if (c.contains(Character.toString(s.charAt(i)))) {
                contains[i] = 1;
            }
        }

        int i = 0;
        int j = 0;
        long res = 0;
        while (i < n && j <= n) {
            if (contains[i] == 0) {
                i++;
                j = i + 1;
            } else {
                if (j < n && contains[j] == 1) {
                    j++;
                } else {
                    long d = j - i;
                    res = res + d * (d + 1) / 2;
                    i = j;
                    j++;
                }
            }
        }
        out.print(res);
    }
}
