package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();
        int length = s.length();
        String hello = "hello";
        int i = 0;
        int j = 0;
        while (i < hello.length()) {
            if (hello.charAt(i) == s.charAt(j)) {
                i++;
            }
            j++;
            if (j == length) {
                break;
            }
        }
        out.println(i == hello.length() ? "YES" : "NO");
    }
}
