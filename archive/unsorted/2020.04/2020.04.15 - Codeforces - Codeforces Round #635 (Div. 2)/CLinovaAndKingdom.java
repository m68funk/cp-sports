package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CLinovaAndKingdom {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int k = in.readInt();

        Foo[] a = new Foo[n];
        int[] degree = new int[n];

        for (int i = 0; i < n - 1; i++) {
            int l = in.readInt();
            degree[l - 1]++;
            int r = in.readInt();
            degree[r - 1]++;
            a[l - 1] = new Foo(r - 1, a[l - 1]);
            a[r - 1] = new Foo(l - 1, a[r - 1]);
        }
        int[] distance = new int[n];
        Arrays.fill(distance,-1);
        Set<Integer> hub = new HashSet<>();
        hub.add(0);
        distance[0] = 0;
        while (!hub.isEmpty()) {
            Integer next = hub.iterator().next();

            Foo f = a[next];
            while (f != null) {
                if (distance[f.val] == -1) {
                    distance[f.val] = distance[next] + 1;
                    hub.add(f.val);
                }
                f = f.next;
            }
            hub.remove(next);
        }

        out.println("foo");

    }
}

class Foo {
    Foo next;
    int val;

    public Foo(int l, Foo foo) {
        val = l;
        next = foo;
    }
}
