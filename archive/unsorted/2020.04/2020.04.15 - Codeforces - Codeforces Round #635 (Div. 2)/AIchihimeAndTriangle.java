package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class AIchihimeAndTriangle {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int a = in.readInt();
            int b = in.readInt();
            int c = in.readInt();
            int d = in.readInt();
            out.println(b + "  " + c + "  " + c);
        }
    }
}
