package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BKanaAndDragonQuestGame {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            int x = in.readInt();
            int n = in.readInt();
            int m = in.readInt();
            if (x >= 20) {
                for (int j = 0; j < n; j++) {
                    x = x / 2 + 10;
                }
            }
            if (x > m * 10) {
                out.println("NO");
            } else {
                out.println("YES");
            }
        }
    }
}
