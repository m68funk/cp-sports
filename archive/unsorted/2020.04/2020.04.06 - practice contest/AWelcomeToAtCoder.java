package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class AWelcomeToAtCoder {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        in.nextLine();
        String s = in.nextLine();
        out.println((a + b + c) + " " + s);
    }
}
