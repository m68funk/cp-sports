package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BInteractiveSorting {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int N = in.nextInt();
        int Q = in.nextInt();

        char[] balls = new char[N];
        for (int i = 0; i < N; i++) {
            balls[i] = (char) ('A' + i);
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N - 1; j++) {
                out.println("?" + balls[j] + balls[j + 1]);
                out.flush();
                String ans = in.next();
                if (ans == ">") {
                    char tmp = balls[j];
                    balls[j] = balls[j + 1];
                    balls[j + 1] = tmp;
                }
            }


        }
        StringBuilder sb = new StringBuilder("!");
        for (int i = 0; i < N; i++) {
            sb.append(balls[i]);
        }
        out.println(sb.toString());
        out.flush();
    }
}
