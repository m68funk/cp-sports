package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] p = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            p[i] = in.readInt();
        }
        int[] trail = new int[n + 1];
        int[] res = new int[n + 1];

        for (int i = 1; i <= n; i++) {
            int j = i;
            trail[i] = j;
            while (trail[p[j]] != i) {
                j = p[j];
                trail[j] = i;
            }
            res[i] = p[j];
        }
        for (int i = 1; i <= n; i++) {
            out.print(res[i] + " ");
        }
    }
}
