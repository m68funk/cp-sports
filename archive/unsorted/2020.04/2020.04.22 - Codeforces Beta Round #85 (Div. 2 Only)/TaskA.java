package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int res = in.readToken().toLowerCase().compareTo(in.readToken().toLowerCase());
        res = res < 0 ? -1 : res;
        res = res > 0 ? 1 : res;
        out.println(res);
    }
}
