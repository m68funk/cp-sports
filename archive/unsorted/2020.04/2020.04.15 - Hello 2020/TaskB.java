package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        List<Integer> first = new ArrayList<>();
        List<Integer> last = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int l = in.readInt();
            boolean asc = false;
            int previous = in.readInt();
            int fe = previous;
            int le = previous;
            for (int j = 1; j < l; j++) {
                int curr = in.readInt();
                asc = asc || previous < curr;
                if (j == l - 1) {
                    le = curr;
                }
                previous = curr;
            }
            if (asc || l == 1) {
                first.add(fe);
                last.add(le);
            }
        }
        first.sort(Comparator.naturalOrder());
        first.f
        last.sort(Comparator.naturalOrder());

        out.println("foo");

    }
}
