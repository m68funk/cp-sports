package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ANewYearAndNaming {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int m = in.readInt();
        String[] s = new String[n];
        String[] t = new String[m];
        for (int i = 0; i < n; i++) {
            s[i] = in.readToken();
        }
        for (int i = 0; i < m; i++) {
            t[i] = in.readToken();
        }
        int q = in.readInt();
        for (int i = 0; i < q; i++) {
            int y = in.readInt();
            out.println(s[(y - 1) % n] + t[(y - 1) % m]);
        }
    }
}
