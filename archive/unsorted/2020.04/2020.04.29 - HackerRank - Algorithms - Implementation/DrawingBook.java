package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class DrawingBook {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int pages = in.readInt();
        int to = in.readInt();
        out.println(Math.min((pages - to) / 2, to / 2));
    }
}
