package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int R = in.readInt();
        int C = in.readInt();
        char[][] field = new char[R][C];
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                field[i][j] = in.readCharacter();
            }
        }
        boolean possible = true;
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                if (field[i][j] == 'W') {
                    if (!test(i - 1, j, R, C, field)) {
                        possible = false;
                        break;
                    }
                    if (!test(i + 1, j, R, C, field)) {
                        possible = false;
                        break;
                    }
                    if (!test(i, j - 1, R, C, field)) {
                        possible = false;
                        break;
                    }
                    if (!test(i, j + 1, R, C, field)) {
                        possible = false;
                        break;
                    }
                }
            }
        }
        out.println(possible ? "Yes" : "No");
        if (possible) {
            for (int i = 0; i < R; i++) {
                for (int j = 0; j < C; j++) {

                    out.write(field[i][j] == '.' ? 'D' : field[i][j]);
                }
                out.println();
            }
        }
    }

    private boolean test(int i, int j, int R, int C, char[][] field) {
        if (i >= 0 && i < R && j >= 0 && j < C) {
            return field[i][j] != 'S';
        } else {
            return true;
        }
    }
}
