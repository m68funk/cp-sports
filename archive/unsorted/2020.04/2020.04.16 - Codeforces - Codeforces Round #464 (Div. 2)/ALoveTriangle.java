package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ALoveTriangle {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int[] f = new int[n];
        for (int i = 0; i < n; i++) {
            f[i] = in.readInt();
        }
        boolean tria = false;
        for (int a = 0; a < n; a++) {
            int b = f[a];
            int c = f[b - 1];
            int fc = f[c - 1];
            if (a + 1 == fc && a + 1 != b && b != c) {
                tria = true;
                break;
            }
        }
        out.println(tria ? "YES" : "NO");
    }
}
