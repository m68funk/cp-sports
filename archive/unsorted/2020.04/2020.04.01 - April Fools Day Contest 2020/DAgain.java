package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class DAgain {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        String s = in.nextLine();
        char c = s.charAt(6);
        out.println(c % 2);
    }
}
