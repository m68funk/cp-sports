package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class ARoadToZero {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.readInt();
        for (int i = 0; i < t; i++) {
            long x = in.readLong();
            long y = in.readLong();
            long a = in.readLong();
            long b = in.readLong();

            long res1 = a * Math.abs(x - y) + b * Math.min(x, y);
            long res2 = a * Math.abs(x - y) + 2 * a * Math.min(x, y);
            out.println(Math.min(res1, res2));
        }
    }
}
