package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;

public class BBinaryPeriod {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int T = in.readInt();
        for (int i = 0; i < T; i++) {
            String t = in.readToken();
            StringBuilder s = new StringBuilder();
            if (t.contains("0") && t.contains("1")) {
                String atom = t.charAt(0) == '0' ? "01" : "10";
                for (int j = 0; j < t.length(); j++) {
                    s.append(atom);
                }
            } else if (t.contains("0")) {
                for (int j = 0; j < t.length(); j++) {
                    s.append("00");
                }
            } else {
                for (int j = 0; j < t.length(); j++) {
                    s.append("11");
                }
            }
            ;
            out.println(s.toString());

        }
    }
}
