package main;

import java.util.*;
import java.io.PrintWriter;

public class CTwoTeamsComposing {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            Set<Integer> set = new HashSet<>();
            Map<Integer, Integer> map = new HashMap<>();


            for (int j = 0; j < n; j++) {
                int a = in.nextInt();
                set.add(a);
                map.put(a, map.getOrDefault(a, 0) + 1);
            }
            int uniques = set.size();

            int maxSame = 0;
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                maxSame = Math.max(maxSame, entry.getValue());
            }
            int res = n / 2;
            res = Math.min(res, uniques);
            res = Math.min(res, maxSame);

            if (uniques == maxSame && uniques + maxSame <= n) {
                res = res - 1;
            }

            out.println(res);
        }
    }
}
