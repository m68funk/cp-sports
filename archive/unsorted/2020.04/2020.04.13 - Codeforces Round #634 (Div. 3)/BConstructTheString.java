package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class BConstructTheString {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int n = in.nextInt();
            int a = in.nextInt();
            int b = in.nextInt();
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < n; j++) {
                sb.append(Character.toChars(97 + j % b));
            }
            out.println(sb);
        }
    }
}
