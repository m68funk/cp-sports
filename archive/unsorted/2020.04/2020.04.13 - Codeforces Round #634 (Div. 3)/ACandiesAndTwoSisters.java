package main;

import java.util.Scanner;
import java.io.PrintWriter;

public class ACandiesAndTwoSisters {
    public void solve(int testNumber, Scanner in, PrintWriter out) {
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            long n = in.nextLong();
            long res = n % 2 == 1 ? n / 2 : n / 2 - 1;
            out.println(res);
        }
    }
}
