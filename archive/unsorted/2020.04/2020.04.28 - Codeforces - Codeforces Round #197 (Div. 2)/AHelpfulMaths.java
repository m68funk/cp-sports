package de.eigenfunk;

import fastIO.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class AHelpfulMaths {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.readToken();
        String[] sa = s.split("\\+");
        Arrays.sort(sa);
        for (int i = 0; i < sa.length; i++) {
            out.print(sa[i]);
            if (i < sa.length - 1) {
                out.print("+");
            }
        }
    }
}
