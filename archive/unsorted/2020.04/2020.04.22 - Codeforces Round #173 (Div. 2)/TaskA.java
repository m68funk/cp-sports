package de.eigenfunk;

import fastIO.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        int res = 0;
        for (int i = 0; i < n; i++) {
            res = in.readToken().contains("+") ? res + 1: res -1;
        }
        out.println(res);
    }
}
